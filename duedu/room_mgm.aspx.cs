﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class room_mgm : System.Web.UI.Page
    {
        room_mgmDLL Serv = new room_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["schoolid"]) )
                    {
                        setting();
                        get_data();
                    }
                    else
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }

                }
            }
        }

        protected void get_data()
        {
            var data = Serv.getRoom(Request.QueryString["schoolid"], ddlgrade.SelectedValue);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        protected void setting()
        {
            ddlgrade.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddlgrade.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddlgrade.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddlgrade.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddlgrade.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddlgrade.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_room_mgm.aspx?schoolid=" + Request.QueryString["schoolid"] + "&grade=" + ddlgrade.SelectedValue);

        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Manage_School.aspx");
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddroomid = (HiddenField)row.FindControl("hddroomid");
            Response.Redirect("~/edit_room_mgm.aspx?roomid=" + hddroomid.Value + "&schoolid=" + Request.QueryString["schoolid"]);
        }

        protected void btnstudent_mapp_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddroomid = (HiddenField)row.FindControl("hddroomid");
            HiddenField hddgrade = (HiddenField)row.FindControl("hddgrade");
            HiddenField hddsub_room = (HiddenField)row.FindControl("hddsub_room");
            Response.Redirect("~/student_mapping.aspx?roomid=" + hddroomid.Value + "&schoolid=" + Request.QueryString["schoolid"] + "&grade=" + hddgrade.Value + "&sub_room=" + hddsub_room.Value);
        }

        protected void GridView_List_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hddadvisor_name = (HiddenField)(e.Row.FindControl("hddadvisor_name"));
                if (hddadvisor_name.Value == " " || hddadvisor_name.Value == "")
                {
                    e.Row.Cells[3].Text = "ยังไม่มีอาจารย์ที่ปรึกษา";

                }
            }
        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.get_data();

        }
    }
}