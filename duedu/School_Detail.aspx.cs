﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class School_Detail : System.Web.UI.Page
    {
        CultureInfo EngCI = new System.Globalization.CultureInfo("en-US");
        CultureInfo ThCI = new System.Globalization.CultureInfo("th-TH");
        SchoolDLL Serv = new SchoolDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    status();
                    get_data();
                }
            }
        }

        protected void get_data()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                var indus = Serv.getSchoolDetailById(Request.QueryString["id"].ToString());
                if (indus.Rows.Count != 0)
                {
                    if (indus.Rows[0]["school_img"].ToString() == "")
                    {
                        Imgschool.ImageUrl = Server.MapPath("~/img/folder_web_upload.png");

                    }
                    else
                    {
                        Imgschool.ImageUrl = indus.Rows[0]["school_img"].ToString();

                    }
                    txtschoolcode.Text = indus.Rows[0]["school_code"].ToString();
                    txtschoolname.Text = indus.Rows[0]["schoolname"].ToString();
                    txtaddressno.Text = indus.Rows[0]["address_no"].ToString();
                    txtroad.Text = indus.Rows[0]["road"].ToString();
                    txtsubdistrict.Text = indus.Rows[0]["subdistrict"].ToString();
                    txtdistrict.Text = indus.Rows[0]["district"].ToString();
                    txtprovince.Text = indus.Rows[0]["province"].ToString();
                    txtzip.Text = indus.Rows[0]["zip"].ToString();
                    txttel.Text = indus.Rows[0]["tel1"].ToString();

                    txtweblink.Text = indus.Rows[0]["web_uri"].ToString();
                    txtapilink.Text = indus.Rows[0]["api_uri"].ToString();

                    Openclass.Text = Convert.ToDateTime(indus.Rows[0]["open_class"].ToString()).ToString("dd/MM/yyyy", ThCI);
                    Closeclass.Text = Convert.ToDateTime(indus.Rows[0]["close_class"].ToString()).ToString("dd/MM/yyy", ThCI);


                    ddlstatus.SelectedValue = indus.Rows[0]["IsActive"].ToString();
                }
                else
                {
                    Response.Redirect("~/Manage_School.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Manage_School.aspx");
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            var pathpic = "";
            var id = Request.QueryString["id"];
            var userid = HttpContext.Current.Session["userid"].ToString();
            if (this.fileupload_pic.HasFile)
            {
                this.fileupload_pic.SaveAs(Server.MapPath("img/" + fileupload_pic.FileName));
                pathpic = ConfigurationManager.AppSettings["link_path"] + "img/" + fileupload_pic.PostedFile.FileName;
            }
            else
            {
                pathpic = Imgschool.ImageUrl;
            }

            if (txtschoolcode.Text != "" && txtschoolname.Text != "" && txtaddressno.Text != "" && txtroad.Text != ""
                && txtsubdistrict.Text != "" && txtdistrict.Text != "" && txtprovince.Text != "" && txtzip.Text != ""
                && txttel.Text != "" && ddlstatus.SelectedValue != "" && txtweblink.Text != "" && txtapilink.Text != "")
            {
                var chk1 = Serv.getDuplicateSchoolCode(txtschoolcode.Text, Request.QueryString["id"].ToString());
                var chk2 = Serv.getDuplicateSchoolName(txtschoolname.Text, Request.QueryString["id"].ToString());

                if (chk1.Rows.Count != 0)
                {
                    POPUPMSG("หมายเลขรหัสโรงเรียนซ้ำ");
                    return;
                }
                else if (chk2.Rows.Count != 0)
                {
                    POPUPMSG("ชื่อโรงเรียนซ้ำ");
                    return;
                }
                else
                {
                    string d1 = Convert.ToDateTime(con_date(Openclass.Text)).ToString("yyyy-MM-dd", EngCI);
                    string d2 = Convert.ToDateTime(con_date(Closeclass.Text)).ToString("yyyy-MM-dd", EngCI);

                    Serv.UpdateSchool(pathpic, txtschoolname.Text, txtaddressno.Text, txtroad.Text, txtsubdistrict.Text, txtdistrict.Text
                        , txtprovince.Text, txtzip.Text, txttel.Text, d1, d2, ddlstatus.SelectedValue, Request.QueryString["id"], txtweblink.Text, txtapilink.Text,
                        HttpContext.Current.Session["userid"].ToString());

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='Manage_School.aspx';", true);

                }
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                return;
            }
        }


        public string con_date(string x)
        {
            string result = "";
            result = x.Substring(6, 4) + "-" + x.Substring(3, 2) + "-" + x.Substring(0, 2);
            return result;
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Manage_School.aspx");
        }

        protected void status()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}