﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="edit_student.aspx.cs" Inherits="duedu.edit_student" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit Student 
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />
            <br />
            <br />
            <div class="row"  style=" margin-left: 30%">
                <asp:Image ID="Image1" runat="server" Width="250px"  />
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ภาพนักเรียน
                </div>
                <div class="col-md-2 col-lg-3"></div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:FileUpload ID="fileupload_student_img" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    โรงเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtschoolname" runat="server" ReadOnly="true" placeholder="" Width="100%" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />



            <%--   <div class="row">
                <div class="col-md-9 col-lg-9">
                    ตำแหน่ง
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlstudent_level" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />--%>

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    รหัสนักเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtstudentid" runat="server" ReadOnly="true" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    Username
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtusername" runat="server" placeholder="Username" ReadOnly="true" Width="100%" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อจริง
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtfname" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    นามสกุล
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtlname" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เบอร์โทรศัพท์
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txttel" runat="server" placeholder="" TextMode="Number" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อผู้ปกครอง
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtcontactpersonname" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เบอร์โทรผู้ปกครอง
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtcontactpersontel" runat="server" TextMode="Number" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    วันเกิด
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtbirthday" runat="server" placeholder="วันเกิด" class="form-control"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtbirthday" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ระดับชั้นศึกษา
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddl" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label13" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    GPA
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtgpa" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger " Width="114px" />
                    <asp:Button ID="btnreset" runat="server" Text="Reset Password" OnClick="btnreset_Click" class="btn btn-info " 
                        OnClientClick="return confirm('คุณต้องการ Reset Password หรือไม่?');"/>
                    <asp:Button ID="btndel" runat="server" Text="Delete" OnClick="btndel_Click" class="btn btn-danger " Width="114px" 
                        OnClientClick="return confirm('คุณต้องการลบหรือไม่?');" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>



</asp:Content>
