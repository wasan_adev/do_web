﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class add_room_mgm : System.Web.UI.Page
    {
        room_mgmDLL Serv = new room_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["schoolid"]) && !string.IsNullOrEmpty(Request.QueryString["grade"]))
                    {
                        setting();

                    }
                    else
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }
                }
            }
        }

        protected void setting()
        {
            txtgrade.Text = "ชั้น ม." + Request.QueryString["grade"];
            txtgrade.ReadOnly = true;

            ddlroom.Items.Insert(0, new ListItem("ห้อง 1", "1"));
            ddlroom.Items.Insert(1, new ListItem("ห้อง 2", "2"));
            ddlroom.Items.Insert(2, new ListItem("ห้อง 3", "3"));
            ddlroom.Items.Insert(3, new ListItem("ห้อง 4", "4"));
            ddlroom.Items.Insert(4, new ListItem("ห้อง 5", "5"));
            ddlroom.Items.Insert(5, new ListItem("ห้อง 6", "6"));
            ddlroom.Items.Insert(6, new ListItem("ห้อง 7", "7"));
            ddlroom.Items.Insert(7, new ListItem("ห้อง 8", "8"));
            ddlroom.Items.Insert(8, new ListItem("ห้อง 9", "9"));
            ddlroom.Items.Insert(9, new ListItem("ห้อง 10", "10"));
            ddlroom.Items.Insert(10, new ListItem("ห้อง 11", "11"));
            ddlroom.Items.Insert(11, new ListItem("ห้อง 12", "12"));
            ddlroom.Items.Insert(12, new ListItem("ห้อง 13", "13"));
            ddlroom.Items.Insert(13, new ListItem("ห้อง 14", "14"));
            ddlroom.Items.Insert(14, new ListItem("ห้อง 15", "15"));
            ddlroom.Items.Insert(15, new ListItem("ห้อง 16", "16"));
            ddlroom.Items.Insert(16, new ListItem("ห้อง 17", "17"));
            ddlroom.Items.Insert(17, new ListItem("ห้อง 18", "18"));
            ddlroom.Items.Insert(18, new ListItem("ห้อง 19", "19"));
            ddlroom.Items.Insert(19, new ListItem("ห้อง 20", "20"));

            var advis = Serv.getAdvisor(Request.QueryString["schoolid"]);
            if (advis.Rows.Count != 0)
            {
                ddladvisor.DataSource = advis;
                ddladvisor.DataTextField = "advisor_name";
                ddladvisor.DataValueField = "personid";
                ddladvisor.DataBind();

                ddladvisor_2.DataSource = advis;
                ddladvisor_2.DataTextField = "advisor_name";
                ddladvisor_2.DataValueField = "personid";
                ddladvisor_2.DataBind();
            }
            else
            {
                ddladvisor.DataSource = null;
                ddladvisor.DataBind();

                ddladvisor_2.DataSource = null;
                ddladvisor_2.DataBind();
            }
            ddladvisor.Items.Insert(0, new ListItem("", ""));
            ddladvisor_2.Items.Insert(0, new ListItem("", ""));

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            var exist = Serv.getRoomByGrade_SunRoom(Request.QueryString["schoolid"], Request.QueryString["grade"], ddlroom.SelectedValue);
            if (exist.Rows.Count != 0)
            {
                POPUPMSG("ชั้นและห้องที่เลือกมีอยู่แล้วในระบบ");
                return;
            }
            else
            {
                string _filename1 = "";
                string _filename2 = "";

                if (fileupload_class_table.HasFile)
                {
                    string filename1 = Path.GetFileName(fileupload_class_table.FileName);
                    FileInfo fi1 = new FileInfo(fileupload_class_table.FileName);
                    string ext = fi1.Extension.ToLower();
                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                        string img_name1 = "~/img_calendar/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                        _filename1 = ConfigurationManager.AppSettings["link_path"] + "img_calendar/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                        fileupload_class_table.SaveAs(Server.MapPath(img_name1));
                    }
                    else
                    {
                        POPUPMSG("ตารางสอนต้องเป็นภาพเท่านั้น");
                        return;
                    }

                }

                if (fileupload_exam_table.HasFile)
                {
                    string filename1 = Path.GetFileName(fileupload_exam_table.FileName);
                    FileInfo fi1 = new FileInfo(fileupload_exam_table.FileName);
                    string ext = fi1.Extension.ToLower();
                    if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                    {
                        string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                        string img_name1 = "~/img_exam/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                        _filename2 = ConfigurationManager.AppSettings["link_path"] + "img_exam/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                        fileupload_exam_table.SaveAs(Server.MapPath(img_name1));
                    }
                    else
                    {
                        POPUPMSG("ตารางสอบต้องเป็นภาพเท่านั้น");
                        return;
                    }
                }

                Serv.Add_room(Request.QueryString["grade"] + "/" + ddlroom.SelectedValue, Request.QueryString["grade"], ddlroom.SelectedValue, _filename1, _filename2, "y",
                    ddladvisor.SelectedValue, HttpContext.Current.Session["userid"].ToString(), Request.QueryString["schoolid"], ddladvisor_2.SelectedValue);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='room_mgm.aspx?schoolid=" +
                    Request.QueryString["schoolid"] + "';", true);
            }

        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/room_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}