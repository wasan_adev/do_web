﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class student_mapping : System.Web.UI.Page
    {
        student_mappingDLL Serv = new student_mappingDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["schoolid"]) && !string.IsNullOrEmpty(Request.QueryString["roomid"])
                        && !string.IsNullOrEmpty(Request.QueryString["grade"]) && !string.IsNullOrEmpty(Request.QueryString["sub_room"]))
                    {
                        get_data();
                        lb_room.Text = "ชั้นมัธยมศึกษาปีที่ " + Request.QueryString["grade"] + "/" + Request.QueryString["sub_room"];
                    }
                    else
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }

                }
            }
        }

        protected void get_data()
        {
            if (GridView_List.Visible == true)
            {
                var data = Serv.getStudent_list(Request.QueryString["roomid"], txtstudent_name.Text);
                if (data.Rows.Count != 0)
                {
                    GridView_List.DataSource = data;
                    GridView_List.DataBind();
                }
                else
                {
                    GridView_List.DataSource = null;
                    GridView_List.DataBind();
                }
            }
            else
            {
                var data = Serv.getStudent_listNotMap(txtstudent_name.Text, Request.QueryString["grade"]);
                if (data.Rows.Count != 0)
                {
                    GridView_list2.DataSource = data;
                    GridView_list2.DataBind();
                }
                else
                {
                    GridView_list2.DataSource = null;
                    GridView_list2.DataBind();
                }
            }

        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/room_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            lb_room.Text = "รายชื่อนักเรียนที่ไม่อยู่ใน ชั้น ม." + Request.QueryString["grade"] + "/" + Request.QueryString["sub_room"];

            GridView_List.Visible = false;
            GridView_list2.Visible = true;

            btnadd.Visible = false;
            btnlist.Visible = true;

            btnback.Visible = false;


            get_data();

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddroommappid = (HiddenField)row.FindControl("hddroommappid");
            Serv.DeleteRoomMapping(hddroommappid.Value);

            get_data();

            POPUPMSG("บันทึกเรียบร้อย");
            return;
        }

        protected void GridView_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_list2.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnlist_Click(object sender, EventArgs e)
        {
            btnback.Visible = true;

            lb_room.Text = "ชั้นมัธยมศึกษาปีที่ " + Request.QueryString["grade"] + "/" + Request.QueryString["sub_room"];

            GridView_List.Visible = true;
            GridView_list2.Visible = false;

            btnadd.Visible = true;
            btnlist.Visible = false;

            get_data();
        }

        protected void btnadd_list_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddpersonid = (HiddenField)row.FindControl("hddpersonid");
            Serv.InserRoomMapping(Request.QueryString["roomid"], hddpersonid.Value, HttpContext.Current.Session["userid"].ToString(), "y");

            lb_room.Text = "ชั้นมัธยมศึกษาปีที่ " + Request.QueryString["grade"] + "/" + Request.QueryString["sub_room"];

            GridView_List.Visible = true;
            GridView_list2.Visible = false;

            btnadd.Visible = true;
            btnlist.Visible = false;

            btnback.Visible = true;

            get_data();

            POPUPMSG("บันทึกเรียบร้อย");
            return;
        }


        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}