﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="add_course.aspx.cs" Inherits="duedu.add_course" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Course 
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />
            <br />
            <br />
            <%--     <div class="row">
                <div class="col-md-9 col-lg-9">
                    รหัสวิชา
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtcourse_code" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />--%>

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อวิชา
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtcourse_name" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชั้นมัธยมศึกษาปีที่
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtgrade" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    วิชาที่มีใบคำศัพท์หรือไม่
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlIsEng" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger " Width="114px" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>



</asp:Content>
