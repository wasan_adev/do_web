﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Manage_School.aspx.cs" Inherits="duedu.Manage_School" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Manage School
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtschoolname" runat="server" placeholder="School name" class="form-control"></asp:TextBox>
                </div>

                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlstatus" Class="btn btn-primary dropdown-toggle" runat="server"></asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Clear" OnClick="btncancel_Click" ForeColor="Black" class="btn btn-default" Width="114px" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <asp:BoundField DataField="school_code" HeaderText="รหัสโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="schoolname" HeaderText="ชื่อโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="status_name" HeaderText="สถานะ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="open_class" HeaderText="วันเปิดภาคเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="จัดการคาบเรียน" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btnclass" runat="server" Text="คาบเรียน" OnClick="btnclass_Click" CssClass="btn btn-info btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="จัดการชั้น/ห้องเรียน" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btnroom" runat="server" Text="ชั้น/ห้องเรียน" OnClick="btnroom_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการวิชา" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btnmanage" runat="server" Text="รายวิชา" OnClick="btnmanage_Click" CssClass="btn btn-danger btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="sch_id_edit" runat="server" Value='<%# Eval("schoolid") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไข" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>
    <style>
        .BoundField {
            background-color: coral;
        }

        .th {
            background-color: coral;
        }
    </style>
</asp:Content>

