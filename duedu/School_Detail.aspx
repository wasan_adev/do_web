﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="School_Detail.aspx.cs" Inherits="duedu.School_Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                School Detail
            </p>
        </div>
    </div>
    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />

            <div class="col-md-12 col-lg-12" style=" margin-left: 30%">
                <asp:Image ID="Imgschool" runat="server"  Width="150px" Height="150px"/>
            </div>
            <br />
            <div class="col-md-6 col-lg-6" style="  margin-top: 1%; margin-bottom: 2%;">
                <asp:FileUpload ID="fileupload_pic" runat="server" />
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    รหัสโรงเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtschoolcode" runat="server" placeholder="School code" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อโรงเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtschoolname" runat="server" placeholder="School name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เลขที่อยู่
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtaddressno" runat="server" placeholder="Address No" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ถนน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtroad" runat="server" placeholder="Road" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ตำบล
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtsubdistrict" runat="server" placeholder="Subdistrict" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    อำเภอ
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtdistrict" runat="server" placeholder="District" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    จังหวัด
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtprovince" runat="server" placeholder="Province" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    รหัสไปรษณีย์
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtzip" runat="server" placeholder="Zip" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เบอร์โทรศัพท์
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txttel" runat="server" placeholder="Tel" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

           <div class="row">
                <div class="col-md-9 col-lg-9">
                    วันที่เปิดภาคเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="Openclass" runat="server"  autocomplete="off"  placeholder="วันที่เปิดภาคเรียน" class="form-control"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1"  runat="server" TargetControlID="Openclass" Format="dd/MM/yyyy" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

             <div class="row">
                <div class="col-md-9 col-lg-9">
                    วันที่ปิดภาคเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="Closeclass" runat="server"  autocomplete="off"  placeholder="วันที่ปิดภาคเรียน" class="form-control"></asp:TextBox>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2"  runat="server" TargetControlID="Closeclass" Format="dd/MM/yyyy"/>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

             <div class="row">
                <div class="col-md-9 col-lg-9">
                    Web Link
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtweblink" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


             <div class="row">
                <div class="col-md-9 col-lg-9">
                    API Link
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtapilink" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label13" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />



            <div class="row">
                <div class="col-md-9 col-lg-9">
                    Status
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlstatus" runat="server" class="form-control"></asp:DropDownList>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger" Width="114px" />
                </div>
            </div>
            <br />

        </div>
    </div>
</asp:Content>
