﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="add_room_mgm.aspx.cs" Inherits="duedu.add_room_mgm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .WindowsStyle .ajax__combobox_inputcontainer .ajax__combobox_textboxcontainer input {
            margin: 0;
            border: solid 1px #7F9DB9;
            border-right: 0px none;
            padding: 1px 0px 0px 5px;
            font-size: 13px;
            height: 18px;
            position: relative;
        }

        .WindowsStyle .ajax__combobox_inputcontainer .ajax__combobox_buttoncontainer button {
            margin: 0;
            padding: 0;
            background-image: url(windows-arrow.gif);
            background-position: top left;
            border: 0px none;
            height: 21px;
            width: 21px;
        }

        .WindowsStyle .ajax__combobox_itemlist {
            border-color: #7F9DB9;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Add Room 
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />
            <br />
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชั้นมัธยมศึกษาปีที่
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtgrade" runat="server" class="form-control"></asp:TextBox>
                    <%--<asp:DropDownList ID="ddlgrade" runat="server" class="form-control"></asp:DropDownList>--%>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ห้องเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlroom" runat="server" class="form-control"></asp:DropDownList>
                    <%--<ajaxToolkit:ComboBox ID="ddlroom" runat="server" AutoCompleteMode="SuggestAppend"  CssClass="WindowsStyle"></ajaxToolkit:ComboBox>--%>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    อาจารย์ที่ปรึกษา 1
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddladvisor" runat="server" class="form-control"></asp:DropDownList>

                    <%--<ajaxToolkit:ComboBox ID="ddladvisor" runat="server" AutoCompleteMode="SuggestAppend"  CssClass="WindowsStyle"></ajaxToolkit:ComboBox>--%>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

             <div class="row">
                <div class="col-md-9 col-lg-9">
                    อาจารย์ที่ปรึกษา 2
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddladvisor_2" runat="server" class="form-control"></asp:DropDownList>

                    <%--<ajaxToolkit:ComboBox ID="ddladvisor" runat="server" AutoCompleteMode="SuggestAppend"  CssClass="WindowsStyle"></ajaxToolkit:ComboBox>--%>
                </div>
                <div class="col-md-1 col-lg-1">

                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ตารางสอน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:FileUpload ID="fileupload_class_table" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ตารางสอบ
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:FileUpload ID="fileupload_exam_table" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger " Width="114px" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>


</asp:Content>
