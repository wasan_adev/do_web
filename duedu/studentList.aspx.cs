﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class studentList : System.Web.UI.Page
    {
        studentListDLL Serv = new studentListDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    setting();
                    get_data();
                }
            }
        }

        protected void setting()
        {
            ddlgrade.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddlgrade.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddlgrade.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddlgrade.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddlgrade.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddlgrade.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));

            var sc = Serv.getSchool();
            if (sc.Rows.Count != 0)
            {
                ddlschool.DataSource = sc;
                ddlschool.DataTextField = "schoolname";
                ddlschool.DataValueField = "schoolid";
                ddlschool.DataBind();
            }
            else
            {
                ddlschool.DataSource = null;
                ddlschool.DataBind();
            }

        }

        protected void get_data()
        {
            var data = Serv.getStudentList(ddlschool.SelectedValue, ddlgrade.SelectedValue, txtstudentname.Text);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_student.aspx");
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddpersonid = (HiddenField)row.FindControl("hddpersonid");
            Response.Redirect("~/edit_student.aspx?personid=" + hddpersonid.Value);
        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.get_data();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string Ext = Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (Ext == ".xls" || Ext == ".xlsx")
                {
                    string Name = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string FilePath = Server.MapPath("~/doc_import/" + Name);
                    FileUpload1.SaveAs(FilePath);
                    FillGridFromExcelSheet(FilePath, Ext, "no");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='studentList.aspx';", true);
                }
                else
                {
                    POPUPMSG("Please upload valid Excel File");
                    return;
                }
            }
        }

        private void FillGridFromExcelSheet(string FilePath, string ext, string isHader)
        {
            int cnt_data = 0;
            int cnt_data_update = 0;
            string connectionString = "";
            if (ext == ".xls")
            {   //For Excel 97-03
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            else if (ext == ".xlsx")
            {    //For Excel 07 and greater
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            connectionString = String.Format(connectionString, FilePath);
            OleDbConnection conn = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmd.Connection = conn;
            //Fetch 1st Sheet Name
            conn.Open();
            DataTable dtSchema;
            dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string ExcelSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
            conn.Close();
            //Read all data of fetched Sheet to a Data Table
            conn.Open();
            cmd.CommandText = "SELECT * From [" + ExcelSheetName + "]";
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(dt);
            conn.Close();

            if (dt.Rows.Count != 0)
            {
                var sh = Serv.getSchoolById(ddlschool.SelectedValue);
                string school_code = sh.Rows[0]["school_code"].ToString();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var exist = Serv.getPersonByStudentId(ddlschool.SelectedValue, dt.Rows[i]["student_id"].ToString());
                    if (exist.Rows.Count != 0)
                    {
                        cnt_data_update = cnt_data_update + 1;
                        Serv.Update_Student2(dt.Rows[i]["fname"].ToString(), dt.Rows[i]["lname"].ToString(), dt.Rows[i]["student_id"].ToString(),
                             dt.Rows[i]["tel"].ToString(), dt.Rows[i]["Contact_person_name"].ToString(), dt.Rows[i]["Contact_person_tel"].ToString(),
                             dt.Rows[i]["birthdate"].ToString(), dt.Rows[i]["Grade"].ToString(), dt.Rows[i]["GPA"].ToString(), exist.Rows[0]["personid"].ToString());

                        if (dt.Rows[i]["Grade"].ToString() != exist.Rows[0]["grade"].ToString())
                        {
                            Serv.del_room_course(exist.Rows[0]["personid"].ToString());

                            var rid = Serv.getRoomByGradeSubRoom(dt.Rows[i]["Grade"].ToString(), dt.Rows[i]["room"].ToString());
                            if (rid.Rows.Count != 0)
                            {
                                Serv.InserRoomMapping(rid.Rows[0]["roomid"].ToString(), exist.Rows[0]["personid"].ToString(),
                                    HttpContext.Current.Session["userid"].ToString(), "y");
                            }
                            else
                            {
                                var add_room = Serv.Add_room(dt.Rows[i]["Grade"].ToString() + "/" + dt.Rows[i]["room"].ToString(), dt.Rows[i]["Grade"].ToString(),
                                    dt.Rows[i]["room"].ToString(), "", "", "y", "", HttpContext.Current.Session["userid"].ToString(), ddlschool.SelectedValue);

                                Serv.InserRoomMapping(add_room.Rows[0]["last_id"].ToString(), exist.Rows[0]["personid"].ToString(),
                                  HttpContext.Current.Session["userid"].ToString(), "y");
                            }
                        }
                    }
                    else
                    {
                        cnt_data = cnt_data + 1;

                        string password = dt.Rows[i]["birthdate"].ToString().Substring(0, 2) + dt.Rows[i]["birthdate"].ToString().Substring(3, 2) +
                            dt.Rows[i]["birthdate"].ToString().Substring(6, 4);

                        var InStu = Serv.Add_Student2(ddlschool.SelectedValue, "", ConfigurationManager.AppSettings["link_path"] + "img/user.png",
                             school_code + dt.Rows[i]["student_id"].ToString(), password, dt.Rows[i]["fname"].ToString(), dt.Rows[i]["lname"].ToString(),
                              dt.Rows[i]["student_id"].ToString(), dt.Rows[i]["tel"].ToString(), dt.Rows[i]["Contact_person_name"].ToString(),
                              dt.Rows[i]["Contact_person_tel"].ToString(), "y", HttpContext.Current.Session["userid"].ToString(), dt.Rows[i]["birthdate"].ToString(),
                              dt.Rows[i]["Grade"].ToString(), "", dt.Rows[i]["GPA"].ToString());

                        var rid = Serv.getRoomByGradeSubRoom(dt.Rows[i]["Grade"].ToString(), dt.Rows[i]["room"].ToString());
                        if (rid.Rows.Count != 0)
                        {
                            Serv.InserRoomMapping(rid.Rows[0]["roomid"].ToString(), InStu.Rows[0]["last_id"].ToString(),
                                HttpContext.Current.Session["userid"].ToString(), "y");
                        }
                        else
                        {
                            var add_room = Serv.Add_room(dt.Rows[i]["Grade"].ToString() + "/" + dt.Rows[i]["room"].ToString(), dt.Rows[i]["Grade"].ToString(),
                                   dt.Rows[i]["room"].ToString(), "", "", "y", "", HttpContext.Current.Session["userid"].ToString(), ddlschool.SelectedValue);

                            Serv.InserRoomMapping(add_room.Rows[0]["last_id"].ToString(), exist.Rows[0]["personid"].ToString(),
                              HttpContext.Current.Session["userid"].ToString(), "y");
                        }
                    }

                }


            }
        }





        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void ddlschool_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.get_data();
        }
    }
}