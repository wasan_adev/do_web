﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class edit_teacher : System.Web.UI.Page
    {
        teacherListDLL Serv = new teacherListDLL();
        date_convert dd_con = new date_convert();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["personid"]))
                    {
                        Response.Redirect("~/studentList.aspx");
                    }
                    else
                    {
                        setting();
                        bind_data();
                    }

                }
            }
        }

        protected void bind_data()
        {



            var p = Serv.getPersonByID(Request.QueryString["personid"]);
            if (p.Rows.Count != 0)
            {
                if (p.Rows[0]["student_img"].ToString() == "")
                {
                    Image1.ImageUrl = "img/logo.png";
                }
                else
                {
                    Image1.ImageUrl = p.Rows[0]["student_img"].ToString();
                }

                var s = Serv.getSchoolById(p.Rows[0]["schoolid"].ToString());
                txtschoolname.Text = s.Rows[0]["schoolname"].ToString();
                //ddlschool.SelectedValue = p.Rows[0]["schoolid"].ToString();
                txtusername.Text = p.Rows[0]["username"].ToString();
                txtfname.Text = p.Rows[0]["fname"].ToString();
                txtlname.Text = p.Rows[0]["lname"].ToString();
                txttel.Text = p.Rows[0]["Tel"].ToString();

                //ddlclass.SelectedValue = p.Rows[0]["grade"].ToString();

                var coru = Serv.getcourseByClass(Request.QueryString["schoolid"]);
                if (coru.Rows.Count != 0)
                {
                    ddlcourse.DataSource = coru;
                    ddlcourse.DataTextField = "course_name";
                    ddlcourse.DataValueField = "courseid";
                    ddlcourse.DataBind();
                }
                else
                {
                    ddlcourse.DataSource = null;
                    ddlcourse.DataBind();
                }
                ddlcourse.Items.Insert(0, new ListItem("", ""));

                ddlcourse.SelectedValue = p.Rows[0]["course_init"].ToString();

            }
            else
            {
                Response.Redirect("~/studentList.aspx");

            }
        }
        protected void setting()
        {

         

            var coru = Serv.getcourseByClass(Request.QueryString["schoolid"]);
            if (coru.Rows.Count != 0)
            {
                ddlcourse.DataSource = coru;
                ddlcourse.DataTextField = "course_name";
                ddlcourse.DataValueField = "courseid";
                ddlcourse.DataBind();
            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }
            ddlcourse.Items.Insert(0, new ListItem("", ""));

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            if (txtusername.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียนในช่อง Username");
                return;
            }
            if (txtfname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อจริง");
                return;
            }
            if (txtlname.Text == "")
            {
                POPUPMSG("กรุณาใส่นามสกุล");
                return;
            }
            if (txttel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทร");
                return;
            }
            if(ddlcourse.SelectedItem.Text == "")
            {
                POPUPMSG("กรุณาเลือกวิชาที่สอน");
                return;
            }

            var p2 = Serv.getPersonByUsername_withOutPersonid(txtusername.Text, Request.QueryString["personid"]);
            if (p2.Rows.Count != 0)
            {
                POPUPMSG("Username ซ้ำ");
                return;
            }

            string _filename1 = "";

            if (fileupload_student_img.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_student_img.FileName);
                FileInfo fi1 = new FileInfo(fileupload_student_img.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    string img_name1 = "~/img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    _filename1 = ConfigurationManager.AppSettings["link_path"] + "img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    fileupload_student_img.SaveAs(Server.MapPath(img_name1));
                }
                else
                {
                    POPUPMSG("กรุณาใส่ไฟล์ภาพเท่านั้น");
                    return;
                }

            }
            else
            {
                _filename1 = Image1.ImageUrl;
            }


            Serv.Update_teacher(_filename1, txtfname.Text, txtlname.Text, "", txttel.Text, HttpContext.Current.Session["userid"].ToString(),
                "", Request.QueryString["personid"],"", ddlcourse.SelectedValue);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='teacherList.aspx';", true);
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/teacherList.aspx");

        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Serv.reset_password_Student("du1234", Request.QueryString["personid"]);
            POPUPMSG("Reset Password เรียบร้อย");
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Serv.del_Student("n", Request.QueryString["personid"]);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='teacherList.aspx';", true);

        }

        protected void ddlclass_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlcourse.Items.Clear();

            var coru = Serv.getcourseByClass(Request.QueryString["schoolid"]);
            if (coru.Rows.Count != 0)
            {
                ddlcourse.DataSource = coru;
                ddlcourse.DataTextField = "course_name";
                ddlcourse.DataValueField = "courseid";
                ddlcourse.DataBind();
            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }
            ddlcourse.Items.Insert(0, new ListItem("", ""));
        }
    }
}