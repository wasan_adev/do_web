﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="student_mapping.aspx.cs" Inherits="duedu.student_mapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Student List /
                <asp:Label ID="lb_room" runat="server" Text="-"></asp:Label>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-8 col-lg-8" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtstudent_name" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                    <asp:Button ID="btnlist" Visible ="false" runat="server" Text="รายการนักเรียน" OnClick="btnlist_Click" ForeColor="Black" class="btn btn-warning" Width="150px" />
                    <asp:Button ID="btnadd" runat="server" Text="เพิ่มนักเรียน" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="114px" />
                    <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" ForeColor="Black" class="btn btn-success" Width="114px" />
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="school-table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3">
                        <Columns>

                            <asp:BoundField DataField="student_name" HeaderText="ชื่อนักเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="student_id" HeaderText="รหัสนักเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Tel" HeaderText="เบอร์ติดต่อ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Contact_person_name" HeaderText="ผู้ติดต่อหลัก" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Contact_person_tel" HeaderText="เบอร์ผู้ติดต่อหลัก" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />


                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddroommappid" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="ลบ" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GridView_list2" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_list2_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" Visible="false">
                        <Columns>

                            <asp:BoundField DataField="student_name" HeaderText="ชื่อนักเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="student_id" HeaderText="รหัสนักเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Tel" HeaderText="เบอร์ติดต่อ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Contact_person_name" HeaderText="ผู้ติดต่อหลัก" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Contact_person_tel" HeaderText="เบอร์ผู้ติดต่อหลัก" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />


                            <asp:TemplateField HeaderText="เพิ่มรายชื่อ">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddpersonid" runat="server" Value='<%# Eval("personid") %>' />
                                    <asp:Button ID="btnadd_list" runat="server" Text="เพิ่มเข้ารายการ" OnClick="btnadd_list_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>


</asp:Content>
