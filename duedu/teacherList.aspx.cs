﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class teacherList : System.Web.UI.Page
    {
        teacherListDLL Serv = new teacherListDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    setting();
                    get_data();
                }
            }
        }

        protected void setting()
        {

            var sc = Serv.getSchool();
            if (sc.Rows.Count != 0)
            {
                ddlschool.DataSource = sc;
                ddlschool.DataTextField = "schoolname";
                ddlschool.DataValueField = "schoolid";
                ddlschool.DataBind();
            }
            else
            {
                ddlschool.DataSource = null;
                ddlschool.DataBind();
            }

        }
        protected void get_data()
        {
            var th = Serv.getTeacherByName(txtteachername.Text);
            if (th.Rows.Count != 0)
            {
                GridView_List.DataSource = th;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_teacher.aspx?schoolid=" + ddlschool.SelectedValue);

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddpersonid = (HiddenField)row.FindControl("hddpersonid");
            Response.Redirect("~/edit_teacher.aspx?personid=" + hddpersonid.Value + "&schoolid=" + ddlschool.SelectedValue);
        }
    }
}