﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="add_classtime_mgm.aspx.cs" Inherits="duedu.add_classtime_mgm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                เพิ่มคาบเรียน
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />
            <br />
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อโรงเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtschoolname" runat="server" placeholder="" ReadOnly="true" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อคาบเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtclasstime_name" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เริ่มเวลา
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <div class="col-md-4 col-lg-4">
                        <asp:TextBox ID="txthh1" runat="server" placeholder="HH"  class="form-control" TextMode="Number" MaxLength="2"></asp:TextBox>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <asp:TextBox ID="txtmm1" runat="server"  placeholder="MM" class="form-control" TextMode="Number" MaxLength="2"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ถึงเวลา
                </div>
                <div class="col-md-3 col-lg-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <div class="col-md-4 col-lg-4">
                        <asp:TextBox ID="txthh2" runat="server" class="form-control"  placeholder="HH" TextMode="Number" MaxLength="2" Width="100%"></asp:TextBox>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <asp:TextBox ID="txtmm2" runat="server" class="form-control"  placeholder="MM" TextMode="Number" MaxLength="2" Width="100%"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />



            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger " Width="114px" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>


</asp:Content>
