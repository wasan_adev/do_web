﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class add_course_mapp : System.Web.UI.Page
    {
        course_mappingDLL Serv = new course_mappingDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["schoolid"]) && string.IsNullOrEmpty(Request.QueryString["grade"]))
                    {
                        Response.Redirect("~/course_mapp.aspx");
                    }
                    else
                    {
                        var s = Serv.getSchoolById(Request.QueryString["schoolid"]);
                        if (s.Rows.Count != 0)
                        {
                            txtschoolname.Text = s.Rows[0]["schoolname"].ToString();
                        }
                        setting();

                    }
                }
            }
        }

        protected void setting()
        {
            ddlday.Items.Insert(0, new ListItem("วันจันทร์", "Mon"));
            ddlday.Items.Insert(1, new ListItem("วันอังคาร", "Tue"));
            ddlday.Items.Insert(2, new ListItem("วันพุธ", "Wen"));
            ddlday.Items.Insert(3, new ListItem("วันพฤหัสบดี", "Thu"));
            ddlday.Items.Insert(4, new ListItem("วันศุกร์", "Fri"));

            ddlgrade.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddlgrade.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddlgrade.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddlgrade.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddlgrade.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddlgrade.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));

            ddlgrade.SelectedValue = Request.QueryString["grade"];

            var sc = Serv.getTeacherBySchool(Request.QueryString["schoolid"], Request.QueryString["grade"]);
            if (sc.Rows.Count != 0)
            {
                ddlteacher.DataSource = sc;
                ddlteacher.DataTextField = "teacherName";
                ddlteacher.DataValueField = "personid";
                ddlteacher.DataBind();
            }
            else
            {
                ddlteacher.DataSource = null;
                ddlteacher.DataBind();
            }

            var _t = Serv.getTeacherByID(ddlteacher.SelectedValue);
            if (_t.Rows.Count != 0)
            {
                var cm = Serv.getCourseNotINMapp_(_t.Rows[0]["course_init"].ToString());
                if (cm.Rows.Count != 0)
                {
                    ddlcourse.DataSource = cm;
                    ddlcourse.DataTextField = "courseName";
                    ddlcourse.DataValueField = "courseid";
                    ddlcourse.DataBind();
                }
                else
                {
                    ddlcourse.DataSource = null;
                    ddlcourse.DataBind();
                }

            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }

            var r = Serv.getRoom(ddlgrade.SelectedValue, Request.QueryString["schoolid"]);
            if (r.Rows.Count != 0)
            {
                ddlroom.DataSource = r;
                ddlroom.DataTextField = "roomname";
                ddlroom.DataValueField = "roomid";
                ddlroom.DataBind();
            }
            else
            {
                ddlroom.DataSource = null;
                ddlroom.DataBind();
            }

            var tc = Serv.getTimeClass(Request.QueryString["schoolid"]);
            if (tc.Rows.Count != 0)
            {
                ddltimeclass.DataSource = tc;
                ddltimeclass.DataTextField = "classtime_name";
                ddltimeclass.DataValueField = "classtime_id";
                ddltimeclass.DataBind();
            }
            else
            {
                ddltimeclass.DataSource = null;
                ddltimeclass.DataBind();
            }

            var _tc = Serv.getTimeClassByID(Request.QueryString["schoolid"], ddltimeclass.SelectedValue);
            if (_tc.Rows.Count != 0)
            {
                txthh1.Text = _tc.Rows[0]["time1"].ToString().Substring(0, 2);
                txtmm1.Text = _tc.Rows[0]["time1"].ToString().Substring(3, 2);

                txthh2.Text = _tc.Rows[0]["time2"].ToString().Substring(0, 2);
                txtmm2.Text = _tc.Rows[0]["time2"].ToString().Substring(3, 2);
            }
            else
            {
                txthh1.Text = "";
                txtmm1.Text = "";

                txthh2.Text = "";
                txtmm2.Text = "";
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txthh1.Text == "")
            {
                txthh1.Focus();
                POPUPMSG("กรุณากรอกชั่วโมงของเวลาเริ่มเรียน");
                return;
            }
            if (txtmm1.Text == "")
            {
                txtmm1.Focus();
                POPUPMSG("กรุณากรอกนาทีของเวลาเริ่มเรียน");
                return;
            }

            if (txthh2.Text == "")
            {
                txthh2.Focus();
                POPUPMSG("กรุณากรอกชั่วโมงของเวลาสิ้นสุดการเรียน");
                return;
            }
            if (txtmm2.Text == "")
            {
                txtmm2.Focus();
                POPUPMSG("กรุณากรอกนาทีของเวลาสิ้นสุดการเรียน");
                return;
            }

            Serv.insert_courseMapp_te(ddlcourse.SelectedValue, ddlteacher.SelectedValue, ddlroom.SelectedValue, HttpContext.Current.Session["userid"].ToString(),
                Request.QueryString["schoolid"], ddlday.SelectedValue, txthh1.Text + ":" + txtmm1.Text, txthh2.Text + ":" + txtmm2.Text);

            var p = Serv.getPersonRoomMapping(ddlroom.SelectedValue);
            if (p.Rows.Count != 0)
            {
                for (int i = 0; i < p.Rows.Count; i++)
                {
                    Serv.insert_courseMapp_st(ddlcourse.SelectedValue, p.Rows[i]["personid"].ToString(), Request.QueryString["schoolid"],
                        ddlroom.SelectedValue, HttpContext.Current.Session["userid"].ToString());
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='course_mapping.aspx';", true);


        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/course_mapping.aspx");

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void ddlteacher_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var cm = Serv.getCourseNotINMapp(ddlteacher.SelectedValue, ddlgrade.SelectedValue, Request.QueryString["schoolid"]);
            //if (cm.Rows.Count != 0)
            //{
            //    ddlcourse.DataSource = cm;
            //    ddlcourse.DataTextField = "courseName";
            //    ddlcourse.DataValueField = "courseid";
            //    ddlcourse.DataBind();
            //}
            //else
            //{
            //    ddlcourse.DataSource = null;
            //    ddlcourse.DataBind();
            //}

            var _t = Serv.getTeacherByID(ddlteacher.SelectedValue);
            if (_t.Rows.Count != 0)
            {
                var cm = Serv.getCourseNotINMapp_(_t.Rows[0]["course_init"].ToString());
                if (cm.Rows.Count != 0)
                {
                    ddlcourse.DataSource = cm;
                    ddlcourse.DataTextField = "courseName";
                    ddlcourse.DataValueField = "courseid";
                    ddlcourse.DataBind();
                }
                else
                {
                    ddlcourse.DataSource = null;
                    ddlcourse.DataBind();
                }

            }
        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {

            var sc = Serv.getTeacherBySchool(Request.QueryString["schoolid"], ddlgrade.SelectedValue);
            if (sc.Rows.Count != 0)
            {
                ddlteacher.DataSource = sc;
                ddlteacher.DataTextField = "teacherName";
                ddlteacher.DataValueField = "personid";
                ddlteacher.DataBind();
            }
            else
            {
                ddlteacher.Items.Clear();
                ddlteacher.DataSource = null;
                ddlteacher.DataBind();
            }

            var r = Serv.getRoom(ddlgrade.SelectedValue, Request.QueryString["schoolid"]);
            if (r.Rows.Count != 0)
            {
                ddlroom.DataSource = r;
                ddlroom.DataTextField = "roomname";
                ddlroom.DataValueField = "roomid";
                ddlroom.DataBind();
            }
            else
            {
                ddlroom.DataSource = null;
                ddlroom.DataBind();
            }

            var _t = Serv.getTeacherByID(ddlteacher.SelectedValue);
            if (_t.Rows.Count != 0)
            {
                var cm = Serv.getCourseNotINMapp_(_t.Rows[0]["course_init"].ToString());
                if (cm.Rows.Count != 0)
                {
                    ddlcourse.DataSource = cm;
                    ddlcourse.DataTextField = "courseName";
                    ddlcourse.DataValueField = "courseid";
                    ddlcourse.DataBind();
                }
                else
                {
                    ddlcourse.Items.Clear();
                    ddlcourse.DataSource = null;
                    ddlcourse.DataBind();
                }

            }
            else
            {
                ddlcourse.Items.Clear();
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }

        }

        protected void ddltimeclass_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tc = Serv.getTimeClassByID(Request.QueryString["schoolid"], ddltimeclass.SelectedValue);
            if (tc.Rows.Count != 0)
            {
                txthh1.Text = tc.Rows[0]["time1"].ToString().Substring(0, 2);
                txtmm1.Text = tc.Rows[0]["time1"].ToString().Substring(3, 2);

                txthh2.Text = tc.Rows[0]["time2"].ToString().Substring(0, 2);
                txtmm2.Text = tc.Rows[0]["time2"].ToString().Substring(3, 2);
            }
            else
            {
                txthh1.Text = "";
                txtmm1.Text = "";

                txthh2.Text = "";
                txtmm2.Text = "";
            }
        }
    }
}