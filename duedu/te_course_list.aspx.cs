﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class te_course_list : System.Web.UI.Page
    {
        te_course_listDLL Serv = new te_course_listDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    get_data();
                }
            }
        }

        protected void get_data()
        {
            var data = Serv.getCourseList(HttpContext.Current.Session["userid"].ToString(), txtcoursename.Text);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnview_Click(object sender, EventArgs e)
        {

            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddroomid = (HiddenField)row.FindControl("hddroomid");
            Response.Redirect("~/te_room_detail.aspx?roomid=" + hddroomid.Value);

        }

        protected void btnset_score_Click(object sender, EventArgs e)
        {

        }

        protected void btn_inclass_Click(object sender, EventArgs e)
        {

        }
    }
}