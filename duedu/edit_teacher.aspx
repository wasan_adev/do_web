﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="edit_teacher.aspx.cs" Inherits="duedu.edit_teacher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Edit Teacher 
            </p>
        </div>
    </div>

    <div class="card">
        <div style="margin-left: 20px; margin-right: 20; margin-right: 20px; margin-top: 20px; margin-bottom: 50px;">
            <br />
            <br />
            <br />
            <div class="row" style="margin-left: 30%">
                <div class="col-md-6 col-lg-6">
                    <asp:Image ID="Image1" runat="server" Width="250px" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ภาพอาจารย์
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:FileUpload ID="fileupload_student_img" runat="server" />
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    โรงเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtschoolname" runat="server" ReadOnly="true" placeholder="" Width="100%" class="form-control"></asp:TextBox>

                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


<%--            <div class="row">
                <div class="col-md-9 col-lg-9">
                    สอนชั้นเรียน
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlclass" AutoPostBack="true" OnSelectedIndexChanged="ddlclass_SelectedIndexChanged"
                        runat="server" class="form-control">
                    </asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />--%>


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    สอนวิชา
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:DropDownList ID="ddlcourse" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    Username
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtusername" runat="server" ReadOnly="true" placeholder="Username" Width="100%" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    ชื่อจริง
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtfname" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    นามสกุล
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txtlname" runat="server" placeholder="" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />

            <div class="row">
                <div class="col-md-9 col-lg-9">
                    เบอร์โทรศัพท์
                </div>
                <div class="col-md-1 col-lg-1">
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:TextBox ID="txttel" runat="server" placeholder="" TextMode="Number" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-1 col-lg-1">
                    <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <br />


            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" class="btn btn-success" Width="114px" />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" class="btn btn-danger " Width="114px" />
                    <asp:Button ID="btnreset" runat="server" Text="Reset Password" OnClick="btnreset_Click" class="btn btn-info "
                        OnClientClick="return confirm('คุณต้องการ Reset Password หรือไม่?');" />
                    <asp:Button ID="btndel" runat="server" Text="Delete" OnClick="btndel_Click" class="btn btn-danger " Width="114px"
                        OnClientClick="return confirm('คุณต้องการลบหรือไม่?');" />
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>



</asp:Content>
