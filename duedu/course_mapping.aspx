﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="course_mapping.aspx.cs" Inherits="duedu.course_mapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <div class="col-md-7 col-lg-7">
                <p style="font-size: 25px; line-height: 1.5;">
                    Course Mapping
                </p>
            </div>
            <div class="col-md-5 col-lg-5">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:FileUpload ID="FileUpload1" class="form-control" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click"
                                ForeColor="Black" class="btn btn-success" Width="100%" />
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="col-md-5 col-lg-5" style="padding-bottom: 10px;">
                        <asp:TextBox ID="txtcoursename" runat="server" placeholder="Course Name" class="form-control" Width="100%"></asp:TextBox>

                    </div>
                    <div class="col-md-3 col-lg-3">
                        <asp:DropDownList ID="ddlschool" Class="btn btn-primary dropdown-toggle" OnSelectedIndexChanged="ddlschool_SelectedIndexChanged"
                            AutoPostBack="true" runat="server" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <asp:DropDownList ID="ddlgrade" OnSelectedIndexChanged="ddlgrade_SelectedIndexChanged" AutoPostBack="true"
                            Class="btn btn-primary dropdown-toggle" runat="server" Width="100%">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-lg-2">
                        <asp:DropDownList ID="ddlroom" Class="btn btn-primary dropdown-toggle" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlroom_SelectedIndexChanged" Width="100%">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 col-lg-12" style="padding-bottom: 10px;">
                    <div class="col-lg-6 col-lg-6">
                        <asp:TextBox ID="txtteachername" runat="server" placeholder="Teacher Name" class="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="100%" />
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <asp:Button ID="btnadd" runat="server" Text="Mapping" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="100%" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="school-table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <%--                            <asp:BoundField DataField="course_code" HeaderText="รหัสวิชา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" /> 
                            </asp:BoundField>--%>
                            <asp:BoundField DataField="course_name" HeaderText="ชื่อวิชา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="roomname" HeaderText="ชั้น" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_day" HeaderText="วันที่เรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_start" HeaderText="เริ่มเวลา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_end" HeaderText="ถึงเวลา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="teacherName" HeaderText="อาจารย์" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="schoolname" HeaderText="ชื่อโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Delete" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddschoolid" runat="server" Value='<%# Eval("schoolid") %>' />
                                    <asp:HiddenField ID="hddcourseid" runat="server" Value='<%# Eval("courseid") %>' />
                                    <asp:HiddenField ID="hddroomid" runat="server" Value='<%# Eval("roomid") %>' />
                                    <asp:HiddenField ID="hddcourse_mappid" runat="server" Value='<%# Eval("id") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="ลบ" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block"
                                        OnClientClick="return confirm('คุณต้องการลบหรือไม่?');" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>



</asp:Content>
