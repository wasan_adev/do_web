﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class course_mapping : System.Web.UI.Page
    {
        course_mappingDLL Serv = new course_mappingDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    setting();
                    get_data();
                }
            }
        }

        protected void get_data()
        {
            var data = Serv.getCourse(txtteachername.Text, ddlgrade.SelectedValue, ddlroom.SelectedValue, ddlschool.SelectedValue, txtcoursename.Text);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }



        protected void setting()
        {
            ddlgrade.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddlgrade.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddlgrade.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddlgrade.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddlgrade.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddlgrade.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));


            var sc = Serv.getSchool();
            if (sc.Rows.Count != 0)
            {
                ddlschool.DataSource = sc;
                ddlschool.DataTextField = "schoolname";
                ddlschool.DataValueField = "schoolid";
                ddlschool.DataBind();
            }
            else
            {
                ddlschool.DataSource = null;
                ddlschool.DataBind();
            }

            var room = Serv.getRoom(ddlgrade.SelectedValue, ddlschool.SelectedValue);
            if (room.Rows.Count != 0)
            {
                ddlroom.DataSource = room;
                ddlroom.DataTextField = "roomname";
                ddlroom.DataValueField = "roomid";
                ddlroom.DataBind();
            }
            else
            {
                ddlroom.DataSource = null;
                ddlroom.DataBind();
            }
        }


        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_course_mapp.aspx?schoolid=" + ddlschool.SelectedValue + "&grade=" + ddlgrade.SelectedValue);
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddschoolid = (HiddenField)row.FindControl("hddschoolid");
            HiddenField hddcourseid = (HiddenField)row.FindControl("hddcourseid");
            HiddenField hddroomid = (HiddenField)row.FindControl("hddroomid");
            HiddenField hddcourse_mappid = (HiddenField)row.FindControl("hddcourse_mappid");

            Serv.del_room_course(hddcourseid.Value, hddroomid.Value, hddschoolid.Value, hddcourse_mappid.Value);
            get_data();

        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            var room = Serv.getRoom(ddlgrade.SelectedValue, ddlschool.SelectedValue);
            if (room.Rows.Count != 0)
            {
                ddlroom.DataSource = room;
                ddlroom.DataTextField = "roomname";
                ddlroom.DataValueField = "roomid";
                ddlroom.DataBind();
            }
            else
            {
                ddlroom.DataSource = null;
                ddlroom.DataBind();
            }

            get_data();

        }

        protected void ddlroom_SelectedIndexChanged(object sender, EventArgs e)
        {
            get_data();
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string Ext = Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (Ext == ".xls" || Ext == ".xlsx")
                {
                    string Name = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string FilePath = Server.MapPath("~/doc_import/" + Name);
                    FileUpload1.SaveAs(FilePath);
                    FillGridFromExcelSheet(FilePath, Ext, "no");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='course_mapping.aspx';", true);
                }
                else
                {
                    POPUPMSG("Please upload valid Excel File");
                    return;
                }
            }
        }

        private void FillGridFromExcelSheet(string FilePath, string ext, string isHader)
        {
            int cnt_data = 0;
            int cnt_data_update = 0;
            string connectionString = "";
            if (ext == ".xls")
            {   //For Excel 97-03
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            else if (ext == ".xlsx")
            {    //For Excel 07 and greater
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            connectionString = String.Format(connectionString, FilePath);
            OleDbConnection conn = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmd.Connection = conn;
            //Fetch 1st Sheet Name
            conn.Open();
            DataTable dtSchema;
            dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string ExcelSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
            conn.Close();
            //Read all data of fetched Sheet to a Data Table
            conn.Open();
            cmd.CommandText = "SELECT * From [" + ExcelSheetName + "]";
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(dt);
            conn.Close();

            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var c = Serv.getCourseBynameGrade(ddlschool.SelectedValue, dt.Rows[i]["course_name"].ToString(), dt.Rows[i]["grade"].ToString());
                    if (c.Rows.Count != 0)
                    {
                        var t = Serv.getTeacherByName(ddlschool.SelectedValue, dt.Rows[i]["teacher_fname"].ToString(), dt.Rows[i]["teacher_lname"].ToString());
                        if (t.Rows.Count != 0)
                        {
                            var r = Serv.getRoomByGradeSubRoom(dt.Rows[i]["grade"].ToString(), dt.Rows[i]["room"].ToString());
                            if (r.Rows.Count != 0)
                            {
                                Serv.insert_courseMapp_te(c.Rows[0]["courseid"].ToString(), t.Rows[0]["personid"].ToString(),
                                    r.Rows[0]["roomid"].ToString(), HttpContext.Current.Session["userid"].ToString(), ddlschool.SelectedValue,
                                    dt.Rows[i]["class_day"].ToString(), dt.Rows[i]["class_start"].ToString(), dt.Rows[i]["class_end"].ToString());


                                var p = Serv.getPersonRoomMapping(r.Rows[0]["roomid"].ToString());
                                if (p.Rows.Count != 0)
                                {
                                    for (int j = 0; j < p.Rows.Count; j++)
                                    {
                                        Serv.insert_courseMapp_st(c.Rows[0]["courseid"].ToString(), p.Rows[i]["personid"].ToString(), ddlschool.SelectedValue,
                                            r.Rows[0]["roomid"].ToString(), HttpContext.Current.Session["userid"].ToString());
                                    }
                                }


                            }
                        }
                    }



                }


            }
        }

        protected void ddlschool_SelectedIndexChanged(object sender, EventArgs e)
        {
            get_data();
        }
    }
}