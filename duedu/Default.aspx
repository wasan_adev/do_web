﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="duedu.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link href="content/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/ready.css" />
    <link rel="stylesheet" href="assets/css/demo.css" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <title>Do Education</title>
    <style>
        .bg-body {
            width: 100%;
            height: 100%;
            background-position: center;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background: url('img/bg.jpg');
        }
        .btn {
            width: 100%;
        }
        .container {
            position: fixed; 
            top: 50%; 
            left: 50%; 
            transform: translate(-50%, -50%);
        }
        /*.card {
            background-color: rgba(255, 106, 0 , 0.5); 
            border-radius: 5px; 
            width: 40%; 
            margin-left: 30%;
        }*/
    </style>
</head>
<body class="bg-body">
    <div class="container">

    <form id="form1" runat="server">
        <div class="row">
                <div class="col-md-12 col-lg-12">
                    <p class="text-center">
                        <img src="img/logo.png" class="logo" style="width: 250px;" />
                    </p>
                </div>
        </div>
        <br />
        <%--<div class="card">--%>
        <br />
         <div class="row">
            <div class="col-md-12 col-lg-12">
            <p class="text-center" style="font-size: 20px; color: #ffffff; font-weight: bold;">
                Do Education Management
            </p>
            </div>
        </div>
        
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6 col-lg-6">
                <asp:TextBox ID="txtusername" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6 col-lg-6">
                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
            </div>
             <div class="col-md-3 col-lg-3"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-md-6 col-lg-6">
                <asp:Button ID="btnsubmit" runat="server" Text="Login" OnClick="btnsubmit_Click" ForeColor="Black" class="btn" BackColor="#f8ca3e" BorderColor="#a78a65" />
            <div class="col-md-3 col-lg-3"></div>
        </div>
        </div>
        <br />
        <%--</div>--%>
    </form>
    </div>
</body>
</html>
