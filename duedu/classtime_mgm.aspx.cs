﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class classtime_mgm : System.Web.UI.Page
    {
        classtime_mgmDLL Serv = new classtime_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["schoolid"]))
                    {
                        get_data();
                    }
                    else
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }

                }
            }

        }

        protected void get_data()
        {
            var data = Serv.getClassTime(Request.QueryString["schoolid"]);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }




        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_classtime_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);

        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddclasstime_id = (HiddenField)row.FindControl("hddclasstime_id");

            Response.Redirect("~/edit_classtime_mgm.aspx?classtime_id=" + hddclasstime_id.Value + "&schoolid=" + Request.QueryString["schoolid"]);
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddclasstime_id = (HiddenField)row.FindControl("hddclasstime_id");


            Serv.delClassTime(hddclasstime_id.Value);
            POPUPMSG("บันทึกเรียบร้อย");
            get_data();
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}