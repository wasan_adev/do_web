﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class vocab_mgm : System.Web.UI.Page
    {
        vocab_mgmDLL Serv = new vocab_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    get_cnt();
                }
            }
        }

        protected void get_cnt()
        {
            var data = Serv.getCntVocab();
            if (data.Rows.Count != 0)
            {
                lbcnt.Text = " : " + Convert.ToInt32(data.Rows[0]["cnt"]).ToString("#,###");
            }
            else
            {
                lbcnt.Text = " : 0";
            }
        }

        protected void get_data()
        {
            var data = Serv.getVocabByName(txtvocab_name.Text);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {

            if (FileUpload1.HasFile)
            {
                tb_upload.Visible = false;
                div_load.Visible = true;
                string Ext = Path.GetExtension(FileUpload1.PostedFile.FileName);
                if (Ext == ".xls" || Ext == ".xlsx")
                {
                    string Name = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string FilePath = Server.MapPath("~/doc_import/" + Name);
                    FileUpload1.SaveAs(FilePath);
                    FillGridFromExcelSheet(FilePath, Ext, "no");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='vocab_mgm.aspx';", true);
                }
                else
                {
                    tb_upload.Visible = true;
                    div_load.Visible = false;
                    POPUPMSG("Please upload valid Excel File");
                    return;
                }
            }
            else
            {
                POPUPMSG("กรุณา Upload File");
                return;
            }
        }

        private void FillGridFromExcelSheet(string FilePath, string ext, string isHader)
        {
            int cnt_data = 0;
            int cnt_data_update = 0;
            string connectionString = "";
            if (ext == ".xls")
            {   //For Excel 97-03
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            else if (ext == ".xlsx")
            {    //For Excel 07 and greater
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source = {0}; Extended Properties = 'Excel 8.0'";
            }
            connectionString = String.Format(connectionString, FilePath);
            OleDbConnection conn = new OleDbConnection(connectionString);
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            cmd.Connection = conn;
            //Fetch 1st Sheet Name
            conn.Open();
            DataTable dtSchema;
            dtSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string ExcelSheetName = dtSchema.Rows[0]["TABLE_NAME"].ToString();
            conn.Close();
            //Read all data of fetched Sheet to a Data Table
            conn.Open();
            cmd.CommandText = "SELECT * From [" + ExcelSheetName + "]";
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(dt);
            conn.Close();

            if (dt.Rows.Count != 0)
            {
                string script = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var exist = Serv.getVocabByName_dup(dt.Rows[i]["vocab"].ToString().Trim());
                    if (exist.Rows.Count != 0)
                    {
                        script = script + script_Update(dt.Rows[i]["vocab"].ToString().Trim(), dt.Rows[i]["n"].ToString().Trim(), dt.Rows[i]["pron"].ToString().Trim(),
                           dt.Rows[i]["adj"].ToString().Trim(), dt.Rows[i]["v"].ToString().Trim(), dt.Rows[i]["adv"].ToString().Trim(), dt.Rows[i]["prep"].ToString().Trim(),
                           dt.Rows[i]["conj"].ToString().Trim(), dt.Rows[i]["idm"].ToString().Trim(), dt.Rows[i]["phra"].ToString().Trim(), dt.Rows[i]["int"].ToString().Trim(),
                           dt.Rows[i]["aux"].ToString().Trim());
                    }
                    else
                    {
                        script = script + script_insert(dt.Rows[i]["vocab"].ToString().Trim(), dt.Rows[i]["n"].ToString().Trim(), dt.Rows[i]["pron"].ToString().Trim(),
                            dt.Rows[i]["adj"].ToString().Trim(), dt.Rows[i]["v"].ToString().Trim(), dt.Rows[i]["adv"].ToString().Trim(), dt.Rows[i]["prep"].ToString().Trim(),
                            dt.Rows[i]["conj"].ToString().Trim(), dt.Rows[i]["idm"].ToString().Trim(), dt.Rows[i]["phra"].ToString().Trim(), dt.Rows[i]["int"].ToString().Trim(),
                            dt.Rows[i]["aux"].ToString().Trim(), "y");
                    }
                }

                Serv.execute_scritpt(script);
            }
        }

        private static string script_insert(string vocab, string n, string pron, string adj, string v, string adv, string prep,
            string conj, string idm, string phra, string _int, string aux, string flagActive)
        {
            string x = "";

            x = " Insert into TblDic(vocab,n,pron,adj,v,adv,prep,conj,idm,phra,int,aux,flagActive) " +
                  " values('" + vocab + "','" + n + "','" + pron + "','" + adj + "','" + v + "','" + adv + "','" + prep + "','" + conj + "','" + idm + "', " +
                  " '" + phra + "','" + _int + "','" + aux + "', '" + flagActive + "') ;  ";

            return x;
        }

        private static string script_Update(string vocab, string n, string pron, string adj, string v, string adv, string prep,
           string conj, string idm, string phra, string _int, string aux)
        {
            string x = "";

            x = " Update TblDic set n = '" + n + "',pron = '" + pron + "',adj = '" + adj + "',v = '" + v + "',adv = '" + adv + "',prep = '" + prep + "', " +
                " conj = '" + conj + "',idm = '" + idm + "',phra = '" + phra + "',int = '" + _int + "', aux = '" + aux + "' " +
                " where vocab = '" + vocab + "' ; ";

            return x;
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            this.get_data();

        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddid = (HiddenField)row.FindControl("hddid");
            Response.Redirect("~/vocab_mgm_detail.aspx?vocabid=" + hddid.Value);
        }
    }
}