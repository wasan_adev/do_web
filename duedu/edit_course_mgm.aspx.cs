﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class edit_course_mgm : System.Web.UI.Page
    {
        courseDLL Serv = new courseDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["schoolid"]) && string.IsNullOrEmpty(Request.QueryString["courseid"])
                         || string.IsNullOrEmpty(Request.QueryString["grade"]))
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }
                    else
                    {
                        getIsEng();
                        bind_data();
                    }
                }
            }

        }

        protected void bind_data()
        {
            var exist = Serv.getCourseByCourseId(Request.QueryString["schoolid"], Request.QueryString["courseid"]);
            if (exist.Rows.Count != 0)
            {
                //txtcourse_code.Text = exist.Rows[0]["course_code"].ToString();
                txtcourse_name.Text = exist.Rows[0]["course_name"].ToString();
                txtgrade.Text = exist.Rows[0]["grade"].ToString();
                txtgrade.ReadOnly = true;
                ddlIsEng.SelectedValue = exist.Rows[0]["IsEng"].ToString();
            }
            else
            {
                Response.Redirect("~/course_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);

            }
        }

        protected void getIsEng()
        {
            ddlIsEng.Items.Insert(0, new ListItem("ไม่มีใบคำศัพท์", "n"));
            ddlIsEng.Items.Insert(1, new ListItem("มีใบคำศัพท์", "y"));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            var x1 = Serv.getCourseByName_withoutID(Request.QueryString["schoolid"], txtcourse_name.Text, Request.QueryString["courseid"], txtgrade.Text);

            if (x1.Rows.Count != 0)
            {
                POPUPMSG("ชื่อวิชานี้มีอยู่แล้วในระบบ");
                return;
            }

            if (txtcourse_name.Text == "" && txtgrade.Text == "")
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                return;
            }
            else
            {
                Serv.Update_Course(txtcourse_name.Text, txtgrade.Text, "", HttpContext.Current.Session["userid"].ToString(), ddlIsEng.SelectedValue,
                    Request.QueryString["courseid"]);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='course_mgm.aspx?schoolid=" + Request.QueryString["schoolid"] + "';", true);
            }


        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/course_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }


    }
}