﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class add_student : System.Web.UI.Page
    {
        studentListDLL Serv = new studentListDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    setting();
                }
            }
        }

        protected void setting()
        {
            ddl.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddl.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddl.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddl.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddl.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddl.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));

            //ddlstudent_level.Items.Insert(0, new ListItem("นักเรียน", ""));
            //ddlstudent_level.Items.Insert(1, new ListItem("หัวหน้านักเรียน", "1"));
            //ddlstudent_level.Items.Insert(2, new ListItem("รองหัวหน้านักเรียน", "2"));

            var sc = Serv.getSchool();
            if (sc.Rows.Count != 0)
            {
                ddlschool.DataSource = sc;
                ddlschool.DataTextField = "schoolname";
                ddlschool.DataValueField = "schoolid";
                ddlschool.DataBind();
            }
            else
            {
                ddlschool.DataSource = null;
                ddlschool.DataBind();
            }
            ddlschool.Items.Insert(0, new ListItem("", ""));

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            if (ddlschool.SelectedItem.Text == "")
            {
                POPUPMSG("กรุณาเลือกโรงเรียน");
                return;
            }
            if (txtstudentid.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียน");
                return;
            }
            if (txtusername.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียนในช่อง Username");
                return;
            }
            if (txtfname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อจริง");
                return;
            }
            if (txtlname.Text == "")
            {
                POPUPMSG("กรุณาใส่นามสกุล");
                return;
            }
            if (txttel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทร");
                return;
            }
            if (txtcontactpersonname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อผู้ปกครอง");
                return;
            }
            if (txtcontactpersontel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทรผู้ปกครอง");
                return;
            }
            if (txtbirthday.Text == "")
            {
                POPUPMSG("กรุณาเลือกวันเกิด");
                return;
            }
            if (ddl.SelectedValue == "")
            {
                POPUPMSG("กรุณาเลือกระดับชั้นศึกษา");
                return;
            }

            var p = Serv.getPersonByStudentId(ddlschool.SelectedValue, txtstudentid.Text);
            if (p.Rows.Count != 0)
            {
                POPUPMSG("รหัสนักเรียนซ้ำ");
                return;
            }

            var p2 = Serv.getPersonByUsername(ddlschool.SelectedValue, txtprefix.Text + txtusername.Text);
            if (p2.Rows.Count != 0)
            {
                POPUPMSG("Username ซ้ำ");
                return;
            }

            string _filename1 = "";

            if (fileupload_student_img.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_student_img.FileName);
                FileInfo fi1 = new FileInfo(fileupload_student_img.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    string img_name1 = "~/img_calendar/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    _filename1 = ConfigurationManager.AppSettings["link_path"] + "img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    fileupload_student_img.SaveAs(Server.MapPath(img_name1));
                }
                else
                {
                    POPUPMSG("ตารางสอนต้องเป็นภาพเท่านั้น");
                    return;
                }

            }

            string password = txtbirthday.Text.Substring(0, 2) + txtbirthday.Text.Substring(3, 2) + txtbirthday.Text.Substring(6, 4);

            Serv.Add_Student(ddlschool.SelectedValue, "", _filename1, txtprefix.Text + txtusername.Text, password, txtfname.Text, txtlname.Text,
                txtstudentid.Text, txttel.Text, txtcontactpersonname.Text, txtcontactpersontel.Text, "y", HttpContext.Current.Session["userid"].ToString(),
                txtbirthday.Text, ddl.SelectedValue, "", txtgpa.Text);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='studentList.aspx';", true);

        }

        public string con_date(string x)
        {
            string result = "";
            result = x.Substring(6, 4) + "-" + x.Substring(3, 2) + "-" + x.Substring(0, 2);
            return result;
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }



        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/studentList.aspx");
        }

        protected void ddlschool_SelectedIndexChanged(object sender, EventArgs e)
        {
            var s = Serv.getSchoolByID(ddlschool.SelectedValue);
            if (s.Rows.Count != 0)
            {
                txtprefix.Text = s.Rows[0]["school_code"].ToString();
            }
        }
    }
}