﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class vocab_mgm_detail : System.Web.UI.Page
    {
        vocab_mgmDLL Serv = new vocab_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["vocabid"]))
                    {
                        Response.Redirect("~/vocab_mgm.aspx");
                    }
                    else
                    {
                        var ex = Serv.getVocabById(Request.QueryString["vocabid"]);
                        if (ex.Rows.Count != 0)
                        {
                            lbvocab.Text = ex.Rows[0]["vocab"].ToString();
                            txtn.Text = ex.Rows[0]["n"].ToString();
                            txtpron.Text = ex.Rows[0]["pron"].ToString();
                            txtadj.Text = ex.Rows[0]["adj"].ToString();
                            txtv.Text = ex.Rows[0]["v"].ToString();
                            txtadv.Text = ex.Rows[0]["adv"].ToString();
                            txtprep.Text = ex.Rows[0]["prep"].ToString();
                            txtconj.Text = ex.Rows[0]["conj"].ToString();
                            txtidm.Text = ex.Rows[0]["idm"].ToString();
                            txtphra.Text = ex.Rows[0]["phra"].ToString();
                            txtint.Text = ex.Rows[0]["int"].ToString();
                            txtaux.Text = ex.Rows[0]["aux"].ToString();
                        }
                    }
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            Serv.execute_scritpt(script_Update(Request.QueryString["vocabid"], txtn.Text, txtpron.Text, txtadj.Text, txtv.Text, txtadv.Text, txtprep.Text, txtconj.Text,
                txtidm.Text, txtphra.Text, txtint.Text, txtaux.Text));

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='vocab_mgm.aspx';", true);
        }


        private static string script_Update(string vocabid, string n, string pron, string adj, string v, string adv, string prep,
           string conj, string idm, string phra, string _int, string aux)
        {
            string x = "";

            x = " Update TblDic set n = '" + n + "',pron = '" + pron + "',adj = '" + adj + "',v = '" + v + "',adv = '" + adv + "',prep = '" + prep + "', " +
                " conj = '" + conj + "',idm = '" + idm + "',phra = '" + phra + "',int = '" + _int + "', aux = '" + aux + "' " +
                " where id = '" + vocabid + "' ; ";

            return x;
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/vocab_mgm.aspx");

        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Serv.delVocab(Request.QueryString["vocabid"]);
        }
    }
}