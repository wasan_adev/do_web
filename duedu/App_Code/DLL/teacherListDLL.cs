﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class teacherListDLL
    {
        public DataTable getSchool()
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblSchool where IsActive = 'y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public DataTable getSchoolById(string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblSchool where IsActive = 'y' and schoolid = '" + schoolid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }




        public DataTable getPersonByID(string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and  personid = '" + personid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getPersonByUsername_withOutPersonid(string username, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and  username = '" + username + "' and personid <> '" + personid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getTeacherByName(string name)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select s.schoolname, concat(p.fname,' ',p.lname) as teacher_name ,p.*,c.course_name  " +
                        " from TblPerson p inner join TblSchool s on p.schoolid = s.schoolid " +
                        " inner join TblCourse c on p.course_init = c.courseid   " +




                        " where person_level = '2'  and concat(p.fname, ' ', p.lname) like '%" + name + "%' and  p.IsActive = 'y'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Add_teacher(string schoolid, string student_img,
            string username, string password, string fname, string lname, string personal_id, string Tel, string IsActive, 
            string create_id, string birthdate,string grade, string course_init)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblPerson(schoolid,person_level,student_level,student_img,username,password,fname,lname,personal_id, " +
                " Tel,IsActive,create_date,create_id,update_id, birthdate, grade, course_init ) " +
                " values('" + schoolid + "', '2','1','" + student_img + "','" + username + "','" + password + "', " +
                " '" + fname + "','" + lname + "','" + personal_id + "', " +
                "  '" + Tel + "','y',getdate(), '" + create_id + "', '" + create_id + "', '" + birthdate + "' , '" + grade + "', '" + course_init + "' ) ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }


        public void Update_teacher(string student_img, string fname, string lname, string personal_id, string Tel, string update_id, string birthdate, string personid,
            string grade, string course_init)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update TblPerson set student_img = '" + student_img + "' , fname = '" + fname + "' , lname = '" + lname + "' , " +
                        " personal_id = '" + personal_id + "' , tel = '" + Tel + "' , update_date = getdate() , update_id = '" + update_id + "', birthdate = '" + birthdate + "', " +
                        " grade = '"+ grade + "', course_init = '"+ course_init + "' " +
                        " where personid = '" + personid + "'  ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }



        public DataTable getPersonByUsername(string schoolid, string username)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and schoolid = '" + schoolid + "'  and username = '" + username + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void reset_password_Student(string password, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set password = '" + password + "' where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }


        public void del_Student(string IsActive, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set IsActive = '" + IsActive + "' where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public DataTable getcourseByClass(string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblCourse where schoolid = '" + schoolid + "' and IsActive  ='y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }



    }
}