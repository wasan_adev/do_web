﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class student_mappingDLL
    {

        public DataTable getStudent_list(string roomid, string name)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select r.*,Concat(p.fname,' ',p.lname) as student_name, p.student_id, p.Tel,p.Contact_person_name,p.Contact_person_tel,rm.roomname " +
                        " from TblRoomMapping r inner " +
                        " join TblPerson p on r.personid = p.personid inner join TblRoom rm on r.roomid = rm.roomid " +
                        " where r.roomid = '" + roomid + "' and r.IsActive = 'y' and  Concat(p.fname,' ',p.lname) like '%" + name + "%' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getStudent_listNotMap(string name,string grade)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select *,Concat(fname,' ',lname) as student_name from TblPerson where grade = '"+ grade + "'  " +
                " and personid not in (select personid from TblRoomMapping where IsActive = 'y') and " +
                " Concat(fname,' ',lname) like '%" + name + "%'  and IsActive = 'y' and person_level = '1'   ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


        public void InserRoomMapping(string roomid, string personid, string create_id, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblRoomMapping(roomid,personid,create_date,create_id,update_date,update_id,IsActive) " +
                    " values('" + roomid + "','" + personid + "',getdate(),'" + create_id + "',getdate(),'" + create_id + "', '" + IsActive + "') ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void DeleteRoomMapping(string id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = "delete from TblRoomMapping where id = '"+ id +"' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }






    }
}