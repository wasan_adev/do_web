﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class studentListDLL
    {
        public DataTable getStudentList(string schoolid, string grade, string name)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select *,concat(p.fname,' ',p.lname) as student_name , s.schoolname, r.roomname " +
                   " from TblPerson p inner " +
                   " join TblSchool s on p.schoolid = s.schoolid " +
                   " left join TblRoomMapping rm on p.personid = rm.personid " +
                   " left join TblRoom r on rm.roomid = r.roomid " +
                    " where p.schoolid = '" + schoolid + "' and p.grade = '" + grade + "' and concat(p.fname,' ',p.lname) like '%" + name + "%' and  p.IsActive = 'y' and p.person_level = '1' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }
    
        public DataTable getSchool()
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblSchool where IsActive = 'y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getSchoolByID(string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from TblSchool where IsActive = 'y' and schoolid = '" + schoolid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getPersonByStudentId(string schoolid, string student_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and schoolid = '" + schoolid + "'  and student_id = '" + student_id + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getPersonByUsername(string schoolid, string username)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and schoolid = '" + schoolid + "'  and username = '" + username + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getPersonByUsername_withOutPersonid(string schoolid, string username, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and schoolid = '" + schoolid + "'  and username = '" + username + "' and personid <> '" + personid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getPersonByID(string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblPerson where IsActive = 'y' and  personid = '" + personid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getSchoolById(string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select top(1)* from TblSchool where IsActive = 'y' and schoolid = '" + schoolid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Add_Student2(string schoolid, string student_level, string student_img, string username, string password, string fname,
            string lname, string student_id, string tel, string Contact_person_name, string Contact_person_tel,
            string IsActive, string create_id, string birthdate, string grade, string personal_id, string GPA)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL = " insert into TblPerson(schoolid,student_level,student_img,username,password,fname,lname,student_id,tel,Contact_person_name,Contact_person_tel, " +
                  " IsActive,create_date,create_id,update_date, update_id,birthdate, person_level, grade ,personal_id, GPA) " +
                  " values('" + schoolid + "','" + student_level + "','" + student_img + "','" + username + "','" + password + "','" + fname + "', " +
                  " '" + lname + "','" + student_id + "','" + tel + "', '" + Contact_person_name + "','" + Contact_person_tel + "', " +
                  " '" + IsActive + "',getdate() ,'" + create_id + "', getdate(), '" + create_id + "', '" + birthdate + "', '1','" + grade + "','" + personal_id + "', " +
                  " '" + GPA + "') ; SELECT @@identity as last_id ;  ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable Add_room(string roomname, string grade, string sub_room, string schedule_img, string exam_img, string IsActive,
          string advisor_id, string create_id, string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL = " Insert into TblRoom(roomname,grade,sub_room,schedule_img,exam_img,IsActive,advisor_id,create_date,create_id,update_date,update_id,schoolid) " +
                    " values('" + roomname + "','" + grade + "','" + sub_room + "','" + schedule_img + "','" + exam_img + "','" + IsActive + "','" + advisor_id + "', getdate() , " +
                    " '" + create_id + "', getdate() ,'" + create_id + "','" + schoolid + "') ; SELECT @@identity as last_id ;  ";



            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

    

        public void Add_Student(string schoolid, string student_level, string student_img, string username, string password, string fname,
            string lname, string student_id, string tel, string Contact_person_name, string Contact_person_tel,
            string IsActive, string create_id, string birthdate, string grade, string personal_id, string GPA)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into TblPerson(schoolid,student_level,student_img,username,password,fname,lname,student_id,tel,Contact_person_name,Contact_person_tel, " +
                    " IsActive,create_date,create_id,update_date, update_id,birthdate, person_level, grade ,personal_id, GPA) " +
                    " values('" + schoolid + "','" + student_level + "','" + student_img + "','" + username + "','" + password + "','" + fname + "', " +
                    " '" + lname + "','" + student_id + "','" + tel + "', '" + Contact_person_name + "','" + Contact_person_tel + "', " +
                    " '" + IsActive + "',getdate() ,'" + create_id + "', getdate(), '" + create_id + "', '" + birthdate + "', '1','" + grade + "','" + personal_id + "', '" + GPA + "') ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public void Update_Student(string student_img, string fname,
            string lname, string student_id, string tel, string Contact_person_name, string Contact_person_tel,
            string birthdate, string grade, string personal_id, string GPA, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set student_img = '" + student_img + "', fname = '" + fname + "', " +
                " lname = '" + lname + "', student_id = '" + student_id + "', tel = '" + tel + "', Contact_person_name = '" + Contact_person_name + "', " +
                " Contact_person_tel = '" + Contact_person_tel + "', birthdate = '" + birthdate + "', grade = '" + grade + "', personal_id = '" + personal_id + "', " +
                " GPA = '" + GPA + "' " +
                " where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public void Update_Student2( string fname, string lname, string student_id, string tel,
            string Contact_person_name, string Contact_person_tel,
            string birthdate, string grade, string GPA, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set  fname = '" + fname + "', " +
                " lname = '" + lname + "', student_id = '" + student_id + "', tel = '" + tel + "', Contact_person_name = '" + Contact_person_name + "', " +
                " Contact_person_tel = '" + Contact_person_tel + "', birthdate = '" + birthdate + "', grade = '" + grade + "', " +
                " GPA = '" + GPA + "' " +
                " where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public void del_room_course(string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " delete from TblRoomMapping where personid = '" + personid + "' ; " +
                    " delete from TblCourseMapping where userid = '" + personid + "' ;  ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public void reset_password_Student(string password, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set password = '" + password + "' where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }


        public void del_Student(string IsActive, string personid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblPerson set IsActive = '" + IsActive + "' where personid = '" + personid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }


        public DataTable getRoomByGradeSubRoom(string grade, string sub_room)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select * from TblRoom where grade = '" + grade + "' and sub_room = '" + sub_room + "'  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void InserRoomMapping(string roomid, string personid, string create_id, string IsActive)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblRoomMapping(roomid,personid,create_date,create_id,update_date,update_id,IsActive) " +
                    " values('" + roomid + "','" + personid + "',getdate(),'" + create_id + "',getdate(),'" + create_id + "', '" + IsActive + "') ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }


    }
}