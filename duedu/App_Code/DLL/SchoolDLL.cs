﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class SchoolDLL
    {
        public DataTable getdataschool(string name, string status)
        {
            string condition = "";
            if (name != "" && status != "")
            {
                condition = "select *, case when IsActive = 'y' then 'Active' else 'Inactive' end as status_name " +
                    " from tblSchool where  schoolname LIKE '%" + name + "%' and IsActive = '" + status + "'";
            }
            else if (name != "")
            {
                condition = "select *, case when IsActive = 'y' then 'Active' else 'Inactive' end as status_name " +
                    " from tblSchool where  schoolname LIKE '%" + name + "%'";
            }
            else if (status != "")
            {
                condition = "select *, case when IsActive = 'y' then 'Active' else 'Inactive' end as status_name " +
                    " from tblSchool where IsActive = '" + status + "'";
            }
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += condition;


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getSchoolDetailById(string Id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblSchool where schoolid = '" + Id + "'  and  IsActive = 'y' ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getDuplicateSchoolCode(string school_code, string id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblSchool where school_code = '" + school_code + "' and schoolid != '" + id + "'  and IsActive = 'y' ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getDuplicateSchoolName(string school_name, string id)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblSchool where schoolname = '" + school_name + "' and schoolid != '" + id + "'  and IsActive = 'y' ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void UpdateSchool(string school_img, string schoolname, string address_no
            , string road, string subdistrict, string district, string province, string zip
            , string tel1, string open_class, string close_class, string IsActive, string schoolid
            , string web_uri, string api_uri,string update_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Update TblSchool set school_img = '" + school_img + "', schoolname = '" + schoolname + "' , address_no = '" + address_no + "' " +
                " , road = '" + road + "' ,subdistrict = '" + subdistrict + "' , district = '" + district + "', province = '" + province + "' , zip = '" + zip + "' " +
                " , tel1 = '" + tel1 + "'  ,open_class = '" + open_class + "',close_class = '" + close_class + "', IsActive = '" + IsActive + "' " +
                " , web_uri = '" + web_uri + "', api_uri = '" + api_uri + "', update_date = getdate(), update_id = '"+ update_id + "' " +
                " where schoolid = '" + schoolid + "' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;

        }

        public void AddSchool(string school_img, string school_code, string schoolname, string address_no, string road, string subdistrict
            , string district, string province, string zip, string tel1, string open_class, string close_class, string IsActive, string web_uri, string api_uri,
            string create_id)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " insert into TblSchool (school_img,school_code,schoolname,address_no ,road ,subdistrict ,district , province, " +
                       " zip ,tel1, open_class, close_class ,IsActive, web_uri, api_uri,create_date,create_id) " +
                       " values('" + school_img + "','" + school_code + "', '" + schoolname + "', '" + address_no + "', '" + road + "', '" + subdistrict + "', '" + district + "', '" + province + "', " +
                       " '" + zip + "', '" + tel1 + "', '" + open_class + "', '" + close_class + "', '" + IsActive + "', '" + web_uri + "','" + api_uri + "',getdate(), " +
                       " '" + create_id + "') ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public DataTable getAddcheckDuplicateSchoolCode(string school_code)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblSchool where school_code = '" + school_code + "'  and  IsActive = 'y';  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getAddcheckDuplicateSchoolName(string schoolname)
        {

            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += "  select * from tblSchool where schoolname = '" + schoolname + "'  and  IsActive = 'y';  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


    }
}