﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class te_course_listDLL
    {

        public DataTable getCourseList(string teacherid, string course_name)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select cm.id,cm.courseid, c.course_name,r.roomid, r.roomname,cm.class_day,cm.class_start,cm.class_end " +
                         " from TblCourseMapping_teacher cm inner join TblRoom r on cm.roomid = r.roomid and cm.IsActive = 'y' " +
                         " inner join TblCourse c on cm.courseid = c.courseid " +
                         " where cm.teacherid = '" + teacherid + "' and c.course_name like '%" + course_name + "%' ;  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }


    }
}