﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace duedu.App_Code.DLL
{
    public class room_mgmDLL
    {

        public DataTable getRoom(string schoolid, string grade)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select r.*,s.schoolname, Concat(p.fname,' ',p.lname) as advisor_name from TblRoom r inner join TblSchool s on r.schoolid = s.schoolid " +
                        " left join TblPerson p on p.personid = r.advisor_id " +
                        " where r.schoolid = '" + schoolid + "' and r.grade = '" + grade + "'  and r.IsActive = 'y'  order by CAST(r.sub_room AS int)  ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getRoomByGrade_SunRoom(string schoolid, string grade, string sub_room)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select r.* from TblRoom r  " +
                        " where r.schoolid = '" + schoolid + "' and r.grade = '" + grade + "' and r.sub_room = '" + sub_room + "'  and r.IsActive = 'y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public DataTable getAdvisor(string schoolid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select *,concat(fname,' ',lname) as advisor_name from TblPerson where schoolid = '" + schoolid + "' and person_level = '2'  and IsActive = 'y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }

        public void Add_room(string roomname, string grade, string sub_room, string schedule_img, string exam_img, string IsActive,
            string advisor_id, string create_id, string schoolid,string advisor_id2)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " Insert into TblRoom(roomname,grade,sub_room,schedule_img,exam_img,IsActive,advisor_id,create_date,create_id,update_date,update_id,schoolid,advisor_id2) " +
                    " values('" + roomname + "','" + grade + "','" + sub_room + "','" + schedule_img + "','" + exam_img + "','" + IsActive + "','" + advisor_id + "', getdate() , " +
                    " '" + create_id + "', getdate() ,'" + create_id + "','" + schoolid + "','" + advisor_id2 + "') ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public void update_room(string roomname, string grade, string sub_room, string schedule_img, string exam_img, string advisor_id, string update_id, string roomid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = null;

            strSQL = " update TblRoom set roomname = '" + roomname + "', grade = '" + grade + "',sub_room = '" + sub_room + "', " +
                 " schedule_img = '" + schedule_img + "',exam_img = '" + exam_img + "', advisor_id = '" + advisor_id + "', update_date = getdate() , " +
                 " update_id = '" + update_id + "' where roomid = '" + roomid + "' ";




            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            objConn.Open();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;

            objCmd.ExecuteNonQuery();

            dtAdapter = null;
            objConn.Close();
            objConn = null;
        }

        public DataTable getRoomByRoomID(string schoolid, string roomid)
        {
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            SqlDataAdapter dtAdapter = new SqlDataAdapter();

            DataSet ds = new DataSet();
            DataTable dt = null;
            string strSQL = "";

            strSQL += " select *  from TblRoom where schoolid = '" + schoolid + "' and roomid = '" + roomid + "'  and IsActive = 'y' ";


            objConn.ConnectionString = ConfigurationManager.ConnectionStrings["Du_ConnectionString"].ToString();
            var _with1 = objCmd;
            _with1.Connection = objConn;
            _with1.CommandText = strSQL;
            _with1.CommandType = CommandType.Text;
            dtAdapter.SelectCommand = objCmd;

            dtAdapter.Fill(ds);
            dt = ds.Tables[0];

            dtAdapter = null;
            objConn.Close();
            objConn = null;

            return dt;
        }




    }
}