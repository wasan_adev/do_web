﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="course_mgm.aspx.cs" Inherits="duedu.course_mgm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Course Management
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtcoursename" runat="server" placeholder="Course name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlgrade" Class="btn btn-primary dropdown-toggle" AutoPostBack="true" OnSelectedIndexChanged="ddlgrade_SelectedIndexChanged"
                        runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="114px" />
                    <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" ForeColor="Black" class="btn btn-success" Width="114px" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="school-table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <asp:BoundField DataField="schoolname" HeaderText="ชื่อโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" HeaderStyle-BackColor="#ffd1b3" />
                            <%--<asp:BoundField DataField="course_code" HeaderText="รหัสวิชา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" HeaderStyle-BackColor="#ffd1b3" />--%>
                            <asp:BoundField DataField="course_name" HeaderText="ชื่อวิชา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" HeaderStyle-BackColor="#ffd1b3" />
                            <asp:BoundField DataField="grade" HeaderText="ชั้น (ม.)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%" HeaderStyle-BackColor="#ffd1b3" />

                            <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddcourseid" runat="server" Value='<%# Eval("courseid") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไข" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btndel" runat="server" Text="ลบ" OnClick="btndel_Click" CssClass="btn btn-danger btn-block"
                                        OnClientClick="return confirm('คุณต้องการลบหรือไม่?');" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>


</asp:Content>
