﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="te_course_list.aspx.cs" Inherits="duedu.te_course_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                รายการห้องที่สอน
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-8 col-lg-8" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtcoursename" runat="server" placeholder="วิชา" class="form-control"></asp:TextBox>
                </div>

                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <asp:BoundField DataField="course_name" HeaderText="วิชาที่สอน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="roomname" HeaderText="ห้องที่สอน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_day" HeaderText="วันที่สอน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_start" HeaderText="เริ่มเวลา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="class_end" HeaderText="สิ้นสุดเวลา" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="เช็คชื่อเข้าสอน" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btn_inclass" runat="server" Text="เช็คชื่อ" OnClick="btn_inclass_Click" CssClass="btn btn-success btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การเก็บคะแนน" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddcourseid" runat="server" Value='<%# Eval("courseid") %>' />
                                    <asp:Button ID="btnset_score" runat="server" Text="จัดการคะแนน" OnClick="btnset_score_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ดูรายชื่อ" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddroomid" runat="server" Value='<%# Eval("roomid") %>' />
                                    <asp:Button ID="btnview" runat="server" Text="รายชื่อนักเรียน" OnClick="btnview_Click" CssClass="btn btn-info btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>
    <style>
        .BoundField {
            background-color: coral;
        }

        .th {
            background-color: coral;
        }
    </style>



</asp:Content>
