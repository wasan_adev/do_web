﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class course_mgm : System.Web.UI.Page
    {
        courseDLL Serv = new courseDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["schoolid"]))
                    {
                        setting();
                        get_data();
                    }
                    else
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }

                }
            }
        }
        protected void setting()
        {
            ddlgrade.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddlgrade.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddlgrade.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddlgrade.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddlgrade.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddlgrade.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));
        }

        protected void get_data()
        {
            var data = Serv.getCourse(Request.QueryString["schoolid"], txtcoursename.Text, ddlgrade.SelectedValue);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/add_course.aspx?schoolid=" + Request.QueryString["schoolid"] + "&grade=" + ddlgrade.SelectedValue);
        }

        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddcourseid = (HiddenField)row.FindControl("hddcourseid");
            Response.Redirect("~/edit_course_mgm.aspx?courseid=" + hddcourseid.Value + "&schoolid=" + Request.QueryString["schoolid"]
                + "&grade=" + ddlgrade.SelectedValue );
        }

        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField hddcourseid = (HiddenField)row.FindControl("hddcourseid");
            Serv.del_Course(hddcourseid.Value);
            get_data();

        }

        protected void btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Manage_School.aspx");

        }

        protected void ddlgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            get_data();

        }
    }
}