﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class Default : System.Web.UI.Page
    {
        LoginDLL Serv = new LoginDLL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtusername.Text != "" && txtpassword.Text != "")
            {
                var u = Serv.getUser(txtusername.Text, txtpassword.Text);
                if (u.Rows.Count != 0)
                {
                    HttpContext.Current.Session["userid"] = u.Rows[0]["personid"].ToString();
                    HttpContext.Current.Session["username"] = u.Rows[0]["username"].ToString();
                    if(u.Rows[0]["person_level"].ToString() == "3")
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }
                    else if(u.Rows[0]["person_level"].ToString() == "2")
                    {
                        var ta = Serv.getTeacher(u.Rows[0]["personid"].ToString());
                        if(ta.Rows.Count != 0)
                        {
                            HttpContext.Current.Session["schoolname"] = ta.Rows[0]["schoolname"].ToString();
                        }
                        Response.Redirect("~/te_course_list.aspx");
                    }
                }
                else
                {
                    POPUPMSG("Username / Password ไม่ถูกต้อง");
                }
            }
            else
            {
                POPUPMSG("กรุณากรอก Username / Password ให้ครบถ้วน");
            }

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}