﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="classtime_mgm.aspx.cs" Inherits="duedu.classtime_mgm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                จัดการคาบเรียน
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-6 col-lg-6" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtclasstime" runat="server" placeholder="ชื่อคาบเรียน" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4 col-lg-4" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="114px" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="school-table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <asp:BoundField DataField="schoolname" HeaderText="ชื่อโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3" />
                            <asp:BoundField DataField="classtime_name" HeaderText="คาบเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3" />
                            <asp:BoundField DataField="time1" HeaderText="เวลาเริ่ม" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3" />
                            <asp:BoundField DataField="time2" HeaderText="เวลาสิ้นสุด" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3" />

                            <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddclasstime_id" runat="server" Value='<%# Eval("classtime_id") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไข" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:Button ID="btndel" runat="server" Text="ลบ" OnClick="btndel_Click" CssClass="btn btn-danger btn-block"
                                        OnClientClick="return confirm('คุณต้องการลบหรือไม่?');" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>


</asp:Content>
