﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class Manage_School : System.Web.UI.Page
    {
        SchoolDLL Serv = new SchoolDLL();
        protected void Page_Load(object sender, EventArgs e)

        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    status();
                    get_data();
                }
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            get_data();
        }
        protected void btnadd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Add_School.aspx");
        }
        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Manage_School.aspx");
        }
        protected void btnedit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            HiddenField sch_id_edit = (HiddenField)row.FindControl("sch_id_edit");

            Response.Redirect("~/School_Detail.aspx?id=" + sch_id_edit.Value);
        }

        protected void btnaction_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/Manage_School.aspx");
        }


        protected void status()
        {
            ddlstatus.Items.Insert(0, new ListItem("Active", "y"));
            ddlstatus.Items.Insert(1, new ListItem("Inactive", "n"));
        }
        protected void get_data()
        {
            var data = Serv.getdataschool(txtschoolname.Text, ddlstatus.SelectedValue);
            if (data.Rows.Count != 0)
            {
                GridView_List.DataSource = data;
                GridView_List.DataBind();
            }
            else
            {
                GridView_List.DataSource = null;
                GridView_List.DataBind();
            }
        }
        protected void GridView_List_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView_List.PageIndex = e.NewPageIndex;
            this.get_data();
        }

        protected void btnmanage_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField sch_id_edit = (HiddenField)row.FindControl("sch_id_edit");
            Response.Redirect("~/course_mgm.aspx?schoolid=" + sch_id_edit.Value);

        }

        protected void btnroom_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField sch_id_edit = (HiddenField)row.FindControl("sch_id_edit");
            Response.Redirect("~/room_mgm.aspx?schoolid=" + sch_id_edit.Value);
        }

        protected void btnclass_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            HiddenField sch_id_edit = (HiddenField)row.FindControl("sch_id_edit");
            Response.Redirect("~/classtime_mgm.aspx?schoolid=" + sch_id_edit.Value);
        }
    }
}