﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class add_course : System.Web.UI.Page
    {
        courseDLL Serv = new courseDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["schoolid"]) || string.IsNullOrEmpty(Request.QueryString["grade"]))
                    {
                        Response.Redirect("~/Manage_School.aspx");
                    }
                    else
                    {
                        getIsEng();
                    }
                }
            }
        }

        protected void getIsEng()
        {
            txtgrade.Text = Request.QueryString["grade"];
            txtgrade.ReadOnly = true;

            ddlIsEng.Items.Insert(0, new ListItem("ไม่มีใบคำศัพท์", "n"));
            ddlIsEng.Items.Insert(1, new ListItem("มีใบคำศัพท์", "y"));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            var c1 = Serv.getCourseByName(Request.QueryString["schoolid"], txtcourse_name.Text, Request.QueryString["grade"]);

            if (c1.Rows.Count != 0)
            {
                POPUPMSG("ชื่อวิชานี้มีอยู่แล้วในระบบ");
                return;
            }


            if (txtcourse_name.Text == "" && txtgrade.Text == "")
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                return;
            }
            else
            {
                Serv.Add_Course(Request.QueryString["schoolid"], txtcourse_name.Text, txtgrade.Text, "", HttpContext.Current.Session["userid"].ToString(),
                    "y", ddlIsEng.SelectedValue);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='course_mgm.aspx?schoolid=" + Request.QueryString["schoolid"] + "';", true);

            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/course_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }





    }
}