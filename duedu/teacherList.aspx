﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="teacherList.aspx.cs" Inherits="duedu.teacherList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="row">
        <div class="col-md-12 col-lg-12"></div>
        <div class="col-md-12 col-lg-12">
            <p style="font-size: 25px; line-height: 1.5;">
                Teacher List
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="row">
                <div class="col-md-7 col-lg-7" style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtteachername" runat="server" placeholder="Student name" class="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2 col-lg-2" style="padding-bottom: 10px;">
                    <asp:DropDownList ID="ddlschool" Class="btn btn-primary dropdown-toggle" runat="server"></asp:DropDownList>
                </div>
                <div class="col-md-3 col-lg-3" style="padding-bottom: 10px;">
                    <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" ForeColor="Black" class="btn btn-info" Width="114px" />
                    <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" ForeColor="Black" class="btn btn-warning" Width="114px" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="school-table">
                <div style="margin: 20px 0px; overflow: scroll">

                    <asp:GridView ID="GridView_List" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        OnPageIndexChanging="GridView_List_PageIndexChanging" UseAccessibleHeader="true"
                        ShowFooter="false" PageSize="50" class="table mt-3" BackColor="White" BorderColor="#FF6600">
                        <Columns>

                            <asp:BoundField DataField="teacher_name" HeaderText="ชื่อคุณครู" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="schoolname" HeaderText="ชื่อโรงเรียน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="course_name" HeaderText="วิชาที่สอน" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                       <%--     <asp:BoundField DataField="grade" HeaderText="ชั้น(ม.)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>--%>
                            <asp:BoundField DataField="Tel" HeaderText="เบอร์โทร" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HeaderStyle-BackColor="#ffd1b3">
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Edit" HeaderStyle-BackColor="#ffd1b3">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hddpersonid" runat="server" Value='<%# Eval("personid") %>' />
                                    <asp:Button ID="btnedit" runat="server" Text="แก้ไข" OnClick="btnedit_Click" CssClass="btn btn-warning btn-block" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="center" CssClass="text-center" />
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>

                </div>

            </div>
        </div>
    </div>



</asp:Content>
