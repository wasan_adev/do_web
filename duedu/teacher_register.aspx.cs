﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class teacher_register : System.Web.UI.Page
    {
        teacherListDLL Serv = new teacherListDLL();
        date_convert date_conn = new date_convert();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    setting();
                }
            }
        }

        protected void setting()
        {
            txtpassword.Text = "du1234";

            var sc = Serv.getSchool();
            if (sc.Rows.Count != 0)
            {
                ddlschool.DataSource = sc;
                ddlschool.DataTextField = "schoolname";
                ddlschool.DataValueField = "schoolid";
                ddlschool.DataBind();
            }
            else
            {
                ddlschool.DataSource = null;
                ddlschool.DataBind();
            }

        

            var coru = Serv.getcourseByClass(ddlschool.SelectedValue);
            if (coru.Rows.Count != 0)
            {
                ddlcourse.DataSource = coru;
                ddlcourse.DataTextField = "course_name";
                ddlcourse.DataValueField = "courseid";
                ddlcourse.DataBind();
            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }
            ddlcourse.Items.Insert(0, new ListItem("", ""));
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (ddlcourse.SelectedItem.Text == "")
            {
                POPUPMSG("กรุณาเลือกวิชาที่สอน");
                return;
            }
            if (txtusername.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียนในช่อง Username");
                return;
            }
            if (txtfname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อจริง");
                return;
            }
            if (txtlname.Text == "")
            {
                POPUPMSG("กรุณาใส่นามสกุล");
                return;
            }
            if (txttel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทร");
                return;
            }


            var p2 = Serv.getPersonByUsername(ddlschool.SelectedValue, txtusername.Text);
            if (p2.Rows.Count != 0)
            {
                POPUPMSG("Username ซ้ำ");
                return;
            }

            string _filename1 = "";

            if (fileupload_student_img.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_student_img.FileName);
                FileInfo fi1 = new FileInfo(fileupload_student_img.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    string img_name1 = "~/img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    _filename1 = ConfigurationManager.AppSettings["link_path"] + "img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    fileupload_student_img.SaveAs(Server.MapPath(img_name1));
                }
                else
                {
                    POPUPMSG("กรุณาใส่ไฟล์ภาพเท่านั้น");
                    return;
                }

            }
            else
            {
                _filename1 = ConfigurationManager.AppSettings["link_path"] + "img/user.png";
            }

            Serv.Add_teacher(ddlschool.SelectedValue, _filename1, txtusername.Text, txtpassword.Text, txtfname.Text, txtlname.Text,
               "", txttel.Text, "y", HttpContext.Current.Session["userid"].ToString(), "", "", ddlcourse.SelectedValue);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='teacher_register.aspx';", true);
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/teacherList.aspx");

        }

        protected void ddlclass_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlcourse.Items.Clear();

            var coru = Serv.getcourseByClass(ddlschool.SelectedValue);
            if (coru.Rows.Count != 0)
            {
                ddlcourse.DataSource = coru;
                ddlcourse.DataTextField = "course_name";
                ddlcourse.DataValueField = "courseid";
                ddlcourse.DataBind();
            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }
            ddlcourse.Items.Insert(0, new ListItem("", ""));
        }

        protected void ddlschool_SelectedIndexChanged(object sender, EventArgs e)
        {
            var coru = Serv.getcourseByClass(ddlschool.SelectedValue);
            if (coru.Rows.Count != 0)
            {
                ddlcourse.DataSource = coru;
                ddlcourse.DataTextField = "course_name";
                ddlcourse.DataValueField = "courseid";
                ddlcourse.DataBind();
            }
            else
            {
                ddlcourse.DataSource = null;
                ddlcourse.DataBind();
            }
            ddlcourse.Items.Insert(0, new ListItem("", ""));
        }
    }
}