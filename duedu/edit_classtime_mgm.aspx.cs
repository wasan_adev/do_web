﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class edit_classtime_mgm : System.Web.UI.Page
    {
        classtime_mgmDLL Serv = new classtime_mgmDLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["schoolid"]) && string.IsNullOrEmpty(Request.QueryString["classtime_id"]))
                    {
                        Response.Redirect("~/classtime_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);
                    }
                    else
                    {
                        var s = Serv.getSchoolById(Request.QueryString["schoolid"]);
                        if (s.Rows.Count != 0)
                        {
                            txtschoolname.Text = s.Rows[0]["schoolname"].ToString();
                        }

                        var ex = Serv.getClassTimeByID(Request.QueryString["schoolid"], Request.QueryString["classtime_id"]);
                        if (ex.Rows.Count != 0)
                        {
                            txtclasstime_name.Text = ex.Rows[0]["classtime_name"].ToString();
                            txthh1.Text = ex.Rows[0]["time1"].ToString().Substring(0, 2);
                            txtmm1.Text = ex.Rows[0]["time1"].ToString().Substring(3, 2);
                            txthh2.Text = ex.Rows[0]["time2"].ToString().Substring(0, 2);
                            txtmm2.Text = ex.Rows[0]["time2"].ToString().Substring(3, 2);
                        }
                        else
                        {
                            Response.Redirect("~/classtime_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);
                        }
                    }
                }
            }

        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (txtclasstime_name.Text != "" && txthh1.Text != "" && txthh2.Text != "" && txtmm1.Text != "" && txtmm2.Text != "" && txtschoolname.Text != "")
            {
                Serv.UpdatelassTime(txtclasstime_name.Text, Convert.ToInt32(txthh1.Text).ToString("0#") + ":" + Convert.ToInt32(txtmm1.Text).ToString("0#"),
                   Convert.ToInt32(txthh2.Text).ToString("0#") + ":" + Convert.ToInt32(txtmm2.Text).ToString("0#"), Request.QueryString["classtime_id"]);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='classtime_mgm.aspx?schoolid=" + Request.QueryString["schoolid"] + "';", true);
            }
            else
            {
                POPUPMSG("กรุณากรอกข้อมูลให้ครบถ้วน");
                return;
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/classtime_mgm.aspx?schoolid=" + Request.QueryString["schoolid"]);

        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}