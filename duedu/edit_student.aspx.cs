﻿using duedu.App_Code.DLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace duedu
{
    public partial class edit_student : System.Web.UI.Page
    {
        studentListDLL Serv = new studentListDLL();
        date_convert dd_con = new date_convert();
        string existing = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (HttpContext.Current.Session["userid"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    if (string.IsNullOrEmpty(Request.QueryString["personid"]))
                    {
                        Response.Redirect("~/studentList.aspx");
                    }
                    else
                    {
                        setting();
                        bind_data();
                    }

                }
            }
        }

        protected void bind_data()
        {
            var p = Serv.getPersonByID(Request.QueryString["personid"]);
            if (p.Rows.Count != 0)
            {
                if (p.Rows[0]["student_img"].ToString() == "")
                {
                    Image1.ImageUrl = "img/logo.png";
                }
                else
                {
                    Image1.ImageUrl = p.Rows[0]["student_img"].ToString();
                }

                var s = Serv.getSchoolById(p.Rows[0]["schoolid"].ToString());
                txtschoolname.Text = s.Rows[0]["schoolname"].ToString();
                //ddlschool.SelectedValue = p.Rows[0]["schoolid"].ToString();
                txtstudentid.Text = p.Rows[0]["student_id"].ToString();
                txtusername.Text = p.Rows[0]["username"].ToString();
                txtfname.Text = p.Rows[0]["fname"].ToString();
                txtlname.Text = p.Rows[0]["lname"].ToString();
                //txtpersonalid.Text = p.Rows[0]["personal_id"].ToString();
                txttel.Text = p.Rows[0]["Tel"].ToString();
                txtcontactpersonname.Text = p.Rows[0]["Contact_person_name"].ToString();
                txtcontactpersontel.Text = p.Rows[0]["Contact_person_tel"].ToString();
                txtbirthday.Text = p.Rows[0]["birthdate"].ToString();
                ddl.SelectedValue = p.Rows[0]["grade"].ToString();
                existing = p.Rows[0]["grade"].ToString();
                txtgpa.Text = p.Rows[0]["GPA"].ToString();
            }
            else
            {
                Response.Redirect("~/studentList.aspx");

            }
        }
        protected void setting()
        {
            ddl.Items.Insert(0, new ListItem("ชั้น ม.1", "1"));
            ddl.Items.Insert(1, new ListItem("ชั้น ม.2", "2"));
            ddl.Items.Insert(2, new ListItem("ชั้น ม.3", "3"));
            ddl.Items.Insert(3, new ListItem("ชั้น ม.4", "4"));
            ddl.Items.Insert(4, new ListItem("ชั้น ม.5", "5"));
            ddl.Items.Insert(5, new ListItem("ชั้น ม.6", "6"));
            
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
         
            if (txtstudentid.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียน");
                return;
            }
            if (txtusername.Text == "")
            {
                POPUPMSG("กรุณาใส่รหัสนักเรียนในช่อง Username");
                return;
            }
            if (txtfname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อจริง");
                return;
            }
            if (txtlname.Text == "")
            {
                POPUPMSG("กรุณาใส่นามสกุล");
                return;
            }
            if (txttel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทร");
                return;
            }
            if (txtcontactpersonname.Text == "")
            {
                POPUPMSG("กรุณาใส่ชื่อผู้ปกครอง");
                return;
            }
            if (txtcontactpersontel.Text == "")
            {
                POPUPMSG("กรุณาใส่เบอร์โทรผู้ปกครอง");
                return;
            }
            if (txtbirthday.Text == "")
            {
                POPUPMSG("กรุณาเลือกวันเกิด");
                return;
            }
            if (ddl.SelectedValue == "")
            {
                POPUPMSG("กรุณาเลือกระดับชั้นศึกษา");
                return;
            }


            string _filename1 = "";

            if (fileupload_student_img.HasFile)
            {
                string filename1 = Path.GetFileName(fileupload_student_img.FileName);
                FileInfo fi1 = new FileInfo(fileupload_student_img.FileName);
                string ext = fi1.Extension.ToLower();
                if (ext == ".jpg" || ext == ".JPG" || ext == ".jpeg" || ext == ".JPEG" || ext == ".png" || ext == ".PNG" || ext == ".bmp" || ext == ".BMP")
                {
                    string xx = DateTime.Now.ToString("yyyyMMddssFFF") + filename1;

                    string img_name1 = "~/img_calendar/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    _filename1 = ConfigurationManager.AppSettings["link_path"] + "img/" + HttpContext.Current.Session["userid"].ToString() + "_" + xx;
                    fileupload_student_img.SaveAs(Server.MapPath(img_name1));
                }
                else
                {
                    POPUPMSG("ตารางสอนต้องเป็นภาพเท่านั้น");
                    return;
                }

            }
            else
            {
                _filename1 = Image1.ImageUrl;
            }


            Serv.Update_Student(_filename1,txtfname.Text ,txtlname.Text , txtstudentid.Text , txttel.Text , txtcontactpersonname.Text,txtcontactpersontel.Text,
                txtbirthday.Text, ddl.SelectedValue,"", txtgpa.Text, Request.QueryString["personid"]);

            if(existing != ddl.SelectedValue)
            {
                Serv.del_room_course(Request.QueryString["personid"]);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='studentList.aspx';", true);


        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/studentList.aspx");
        }

        private void POPUPMSG(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert(\'");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("\'", "\\\'"));
            sb.Append("\');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            string password = txtbirthday.Text.Substring(0, 2) + txtbirthday.Text.Substring(3, 2) + txtbirthday.Text.Substring(6, 4);
            Serv.reset_password_Student(password, Request.QueryString["personid"]);
            POPUPMSG("Reset Password เรียบร้อย");
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            Serv.del_Student("n", Request.QueryString["personid"]);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('บันทึกเรียบร้อย');window.location ='studentList.aspx';", true);

        }
    }
}